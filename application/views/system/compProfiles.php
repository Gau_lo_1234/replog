<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1><?php echo lang('companyProfileListingHeading');?></h1>

				<div class="menuReturn btn btn-default">
					<a href="<?php echo base_url('dashboard')?>">Return To Dashboard</a>
				</div>
				<?php if($this->ion_auth->is_superadmin()):?>
				<div class="menuReturnLeft btn btn-default">
					<a href="<?php echo base_url('CompanyProfileAdmin/createCompanyProfile')?>">Create New</a>
				</div>
			<?php endif;?>
			</div>
			<div class="panel-body">
				<?php if($show_inactive == true):?>
					<a href="<?php echo base_url('CompanyProfileAdmin/compProfiles/0')?>">Hide In-Active Profiles</a>
				<?php else:?>
					<a href="<?php echo base_url('CompanyProfileAdmin/compProfiles/1')?>">Show In-Active Profiles</a>
				<?php endif;?>
				
				<?php if(isset($message)):?>
				<div id="infoMessage"><?php echo $message;?></div>
			<?php endif;?>
			</div>
			  <!-- Table -->
				  <table class="table">
						
							<tr>
								<th>Company Name</th>
								<th>Branches</th>
								<th>Created</th>
								<th>Current Status</th>
								<th>Actions</th>
							</tr>
							<?php if(!empty($results)):?>
							<?php foreach ($results as $result):?>
								<tr>
									<td><?php echo $result->companyName;?></td>
									<td><?php echo $result->branchCount;?></td>
									<td><?php echo date('Y-m-d H:i:s',strtotime($result->created));?></td>
									<td>
										<?php if($result->status == true):?>
										Active
										<?php else:?>
										In-Active
										<?php endif;?>
										

									</td>
									<td>
										<!-- list actions available -->
										<a id="view" name="view" href="<?php echo base_url('CompanyProfileAdmin/viewProfile/'.$result->companyID)?>">View</a>
										&nbsp;&nbsp;|&nbsp;&nbsp;

										<a id="edit" name="edit" href="<?php echo base_url('CompanyProfileAdmin/editCompanyProfile/'.$result->companyID)?>">Edit</a>
										&nbsp;&nbsp;|&nbsp;&nbsp;
										<a id="edit" name="edit" href="<?php echo base_url('CompanyProfileAdmin/companyBranchListing/'.$result->companyID)?>">Branches</a>
										
		
										<?php if($this->ion_auth->is_superadmin()):?>
										&nbsp;&nbsp;|&nbsp;&nbsp;					
										<?php if($result->status == true):?>
											<a id="status" name="status" href="<?php echo base_url('CompanyProfileAdmin/changeProfileStatus/'.$result->companyID.'/0')?>">Disable</a>
										<?php else:?>
											<a id="status" name="status" href="<?php echo base_url('CompanyProfileAdmin/changeProfileStatus/'.$result->companyID.'/1')?>">Enable</a>
										<?php endif;?>
									<?php endif;?>
									</td>
								</tr>
							<?php endforeach;?>
							<?php else:?>
								<tr>
									<td colspan="4">
										<h3>No Records Found</h3>
									</td>
								</tr>
						<?php endif;?>
				  </table>
				  
		</div>
	</div>
</div>









