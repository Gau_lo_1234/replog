<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1><?php echo lang('systemrepairWorkDoneListingHeading');?></h1>

				<div class="menuReturn btn btn-default">
					<a href="<?php echo base_url('dashboard')?>">Return To Dashboard</a>
				</div>
				<div class="menuReturnLeft btn btn-default">
					<a href="<?php echo base_url('SystemAdmin/createrepairWorkDone')?>">Create New</a>
				</div>
			</div>
			<div class="panel-body">
				<?php if($show_inactive == true):?>
					<a href="<?php echo base_url('SystemAdmin/repairWorkDoneListing/0')?>">Hide In-Active Work Done Items</a>
				<?php else:?>
					<a href="<?php echo base_url('SystemAdmin/repairWorkDoneListing/1')?>">Show In-Active Work Done Items</a>
				<?php endif;?>
								&nbsp;&nbsp;<span>There are currently <?php echo $inactiveCount; ?> In-Active Work Done Items. </span>
				<?php if(isset($message)):?>
				<div id="infoMessage"><?php echo $message;?></div>
			<?php endif;?>
			</div>
			  <!-- Table -->
				  <table class="table">
						
							<tr>
								<th>Work Done</th>
								<th>Work Done Cost</th>
								<th>Link To Category</th>
								<th>Link To Fault</th>
								<th>Created</th>
								<th>Current Status</th>
								<th>Actions</th>
							</tr>
							<?php if(!empty($results)):?>
							<?php foreach ($results as $result):?>
								<tr>
									<td><?php echo $result->workDoneName;?></td>
									<td><?php echo "R ".number_format($result->workDoneCost,2);?></td>
									<td><?php echo $result->categoryName;?></td>
									<td><?php echo $result->faultName;?></td>
									<td><?php echo date('Y-m-d H:i:s',strtotime($result->created));?></td>
									<td>
										<?php if($result->status == true):?>
										Active
										<?php else:?>
										In-Active
										<?php endif;?>
										

									</td>
									<td>
										<!-- list actions available -->
										<a id="view" name="view" href="<?php echo base_url('SystemAdmin/viewrepairWorkDone/'.$result->workDoneID)?>">View</a>
										&nbsp;&nbsp;|&nbsp;&nbsp;

										<a id="edit" name="edit" href="<?php echo base_url('SystemAdmin/editrepairWorkDone/'.$result->workDoneID)?>">Edit</a>
										&nbsp;&nbsp;|&nbsp;&nbsp;
										<?php if($result->status == true):?>
											<a id="status" name="status" href="<?php echo base_url('SystemAdmin/changerepairWorkDoneStatus/'.$result->workDoneID.'/0')?>">Disable</a>
										<?php else:?>
											<a id="status" name="status" href="<?php echo base_url('SystemAdmin/changerepairWorkDoneStatus/'.$result->workDoneID.'/1')?>">Enable</a>
										<?php endif;?>
									</td>
								</tr>
							<?php endforeach;?>
							<?php else:?>
								<tr>
									<td colspan="4">
										<h3>No Records Found</h3>
									</td>
								</tr>
						<?php endif;?>
				  </table>
				  
		</div>
	</div>
</div>









