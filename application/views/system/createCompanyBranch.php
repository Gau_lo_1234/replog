<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo lang('companyBranchCreateHeading');?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url("CompanyProfileAdmin/companyBranchListing/$companyID")?>">Return To List</a>
        </div>
      </div>
      <div class="panel-body">
        <?php if(isset($message)):?>
        <div id="infoMessage"><?php echo $message;?></div>
      <?php endif;?>
        <!-- Table -->


           <form method="post" action="<?php echo base_url('CompanyProfileAdmin/createBranchProfile')?>">

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="branchName">Branch Name:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="branchName" name="branchName"  placeholder="Branch Name"  size="50" required/>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="branchContactName">Contact Name:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="branchContactName" name="branchContactName"  placeholder="Branch Contact Name"  size="50" required/>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="branchContactTel">Contact Telephone:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="branchContactTel" name="branchContactTel"  placeholder="Contact Telephone"  size="50" required/>
                  </div>

                </div>
                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="branchContactEmail">Contact E-Mail Address:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="branchContactEmail" name="branchContactEmail"  placeholder="E-mail Address"  size="50" required/>
                  </div>

                </div>


                <div class="row">

                  <input type="hidden" name="companyID" value="<?php echo $companyID?>"/>
                  <div class="col-sm-3 inputStyle" >
                     <button class="btn btn-primary" id="submit" name="submit" type="submit">Submit</button>
                  </div>
                  <div class="col-sm-9" >
                    
                  </div>
                </div>


            </form>
    </div>
  </div>
</div>