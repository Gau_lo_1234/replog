<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo lang('systemCreateCompanyProfile');?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url('CompanyProfileAdmin/compProfiles')?>">Return To List</a>
        </div>
      </div>
      <div class="panel-body">
        <?php if(isset($message)):?>
        <div id="infoMessage"><?php echo $message;?></div>
      <?php endif;?>
        <!-- Table -->


           <form method="post" action="<?php echo base_url('CompanyProfileAdmin/createCompanyProfile')?>">

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="companyName">Company Name:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="companyName" name="companyName"  placeholder="Company Name"  size="50" required/>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="companyContactName">Contact Name:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="companyContactName" name="companyContactName"  placeholder="Contact Name"  size="50" required/>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="companyContactTel">Contact Telephone:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="companyContactTel" name="companyContactTel"  placeholder="Contact Telephone"  size="50" required/>
                  </div>

                </div>
                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="companyContactEmail">Contact E-Mail Address:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="companyContactEmail" name="companyContactEmail"  placeholder="E-mail Address"  size="50" required/>
                  </div>

                </div>
                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="branchName">Default Branch Name:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="branchName" name="branchName"  placeholder="Default Branch Name"  size="50" required/>
                  </div>

                </div>


                <div class="row">
                    <div class="col-sm-12">
                      <div class="panel .panel-info">
                        <p>Please Note*</p>
                        <p>The Branch Name is required as it will be used to setup the default branch profile for the company.<br/>
                      </panel>
                    </div>
                </div>

                <div class="row">

                  
                  <div class="col-sm-3 inputStyle" >
                     <button class="btn btn-primary" id="submit" name="submit" type="submit">Submit</button>
                  </div>
                  <div class="col-sm-9" >
                    
                  </div>
                </div>


            </form>
    </div>
  </div>
</div>