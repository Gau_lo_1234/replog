<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo lang('systemViewCompanyProfileHeading');?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url('CompanyProfileAdmin/compProfiles')?>">Return To Company Profile Listing</a>
        </div>
      </div>
      <div class="panel-body">


        <div class="container-fluid viewTableLayout">
          <div class="row">
            <div class="col-sm-4">Company Name:</div>
            <div class="col-sm-8"><?php echo $result->companyName;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Contact Person:</div>
            <div class="col-sm-8"><?php echo $result->companyContactName;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Contact Telephone:</div>
            <div class="col-sm-8"><?php echo $result->companyContactTel;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Contact E-Mail:</div>
            <div class="col-sm-8"><?php echo $result->companyContactEmail;?></div>
          </div>                              
          <div class="row">
            <div class="col-sm-4">Created:</div>
            <div class="col-sm-8"><?php echo $result->created;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Created By:</div>
            <div class="col-sm-8"><?php echo $result->username;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Last Updated:</div>
            <div class="col-sm-8"><?php echo $result->updatedbyuser." on ".$result->lastUpdated;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Current Status:</div>
            <?php if($result->status == true):?>
                <div class="col-sm-8">Active</div>
          <?php else:?>
                <div class="col-sm-8">In-Active</div>
          <?php endif;?>

          </div>
          <div class="row">
            <div class="col-sm-4">Branches:</div>
            <?php if($branches && !empty($branches)):?>
            <div class="col-sm-8">
            <!-- loop through branches array and display items -->
                <?php foreach($branches as $branch):?>
                <p><?php echo $branch->branchName;?></p>
              <?php endforeach;?>
              </div>
          <?php endif;?>

          </div>   
        </div>

    </div>
  </div>
</div>




