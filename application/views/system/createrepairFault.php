<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo lang('systemCreaterepairFaultHeading');?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url('SystemAdmin/repairFaultListing')?>">Return To Repair Fault Listing</a>
        </div>
      </div>
      <div class="panel-body">
        <?php if(isset($message)):?>
        <div id="infoMessage"><?php echo $message;?></div>
      <?php endif;?>
        <!-- Table -->

           <form id="postform" method="post" action="<?php echo base_url('SystemAdmin/createrepairFault')?>">

                <div class="row formRowSpacing">
                  <div class="col-sm-4" >
                     <label for="categoryID"><?php echo lang("selectrepairCategoryHeading") ?></label>
                    
                  </div>
                  <div class="col-sm-8" >
                    <select name="categoryID" id="categoryID">
                      <option value="" default>Please select Repair Category</option>
                      
                      <?php foreach($categorySelect as $val):?>
                          <option value="<?php echo $val->categoryID;?>"><?php echo $val->categoryName;?></option>

                    <?php endforeach;?>
                    </select>
                  </div>

                </div>


                <div class="row formRowSpacing">
                  <div class="col-sm-4" >
                     <label for="faultName">Fault:</label>
                    
                  </div>
                  <div class="col-sm-8" >
                    <input type="text" id="faultName" name="faultName"  placeholder="Fault"  size="50" required/>
                  </div>

                </div>

                <div class="row">

                  
                  <div class="col-sm-3 inputStyle" >
                     <button class="btn btn-primary" id="submit" name="submit" type="submit">Submit</button>
                  </div>
                  <div class="col-sm-9" >
                    
                  </div>
                </div>


            </form>
    </div>
  </div>
</div>


<script>
  $(document).ready(function(){

    $('#postform').submit(function(e) {

      if($('#categoryID option:selected').val() == ''){

        e.preventDefault();
        alert("Please select a Category to Link this Fault To");
      }
    });

  });



</script>