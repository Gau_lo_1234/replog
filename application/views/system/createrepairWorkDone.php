<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo lang('systemCreaterepairWorkDoneHeading');?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url('SystemAdmin/repairWorkDoneListing')?>">Return To Repair Work Done Listing</a>
        </div>
      </div>
      <div class="panel-body">
        <?php if(isset($message)):?>
        <div id="infoMessage"><?php echo $message;?></div>
      <?php endif;?>
        <!-- Table -->

           <form id="postform" method="post" action="<?php echo base_url('SystemAdmin/createrepairWorkDone')?>">

                <div class="row formRowSpacing">
                  <div class="col-sm-4" >
                     <label for="categoryID"><?php echo lang("selectrepairCategoryHeading") ?></label>
                    
                  </div>
                  <div class="col-sm-8" >
                    <select name="categoryID" id="categoryID">
                      <option value="" default>Please select Repair Category</option>
                      
                      <?php foreach($categories as $val):?>
                          <option value="<?php echo $val->categoryID;?>"><?php echo $val->categoryName;?></option>

                    <?php endforeach;?>
                    </select>
                  </div>

                </div>


                <div id="faultListing" class="row formRowSpacing" style="display:none;">
                  <div class="col-sm-4" >
                     <label for="categoryID"><?php echo lang("selectrepairFaultHeading") ?></label>
                    
                  </div>
                  <div class="col-sm-8" >
                    <select name="faultID" id="faultID">
                      <option value="" default>Please select Repair Fault</option>
                      
                      <?php foreach($faults as $val):?>
                          <option value="<?php echo $val->faultID;?>"><?php echo $val->faultName;?></option>

                    <?php endforeach;?>
                    </select>
                  </div>

                </div>


                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="workDoneName">Work Done:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="workDoneName" name="workDoneName"  placeholder="Work Done"  size="50" required/>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="workDoneName">Work Done Cost:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="workDoneCost" name="workDoneCost"  placeholder="Work Done Cost"  size="50" required/>
                  </div>

                </div>

                <div class="row">

                  
                  <div class="col-sm-3 inputStyle" >
                     <button class="btn btn-primary" id="submit" name="submit" type="submit">Submit</button>
                  </div>
                  <div class="col-sm-9" >
                    
                  </div>
                </div>


            </form>
    </div>
  </div>
</div>


<script>
  $(document).ready(function(){

    var faultArray = <?php echo json_encode($faults);?>;



    $('#categoryID').change(function(){
  

      if( $('#categoryID').val() ) { 

        selectedCategory = $('#categoryID option:selected').val();


        var selectbox = $('#faultID');
        selectbox.empty();
        faultSelectList = "<option value=''>Please select Fault Item</option>";
           $.each(faultArray, function(i, item) {
              if(item.categoryID == selectedCategory){
                faultSelectList += "<option value='"+item.faultID+"'>"+item.faultName+"</option>";
              }
          });
        selectbox.html(faultSelectList);


               $('#faultListing').show();

      }else{
        $('#faultListing').hide();
      }
    })
    $('#postform').submit(function(e) {

      if(!$('#categoryID').val()){
        alert("Please select a Category to Link this Item To");
        e.preventDefault();

      }


      if(!$('#faultID').val()){
        alert("Please select a Fault to Link this Item To");
        e.preventDefault();

      }


      if(isNaN($('#workDoneCost').val())){
        alert('Work Done Cost must be a numeric value!');
        e.preventDefault();
      }

    });

  });

</script>