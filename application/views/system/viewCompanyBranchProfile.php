<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo lang('systemViewCompanyBranchHeading');?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url('CompanyProfileAdmin/companyBranchListing/'.$companyID)?>">Return To Company Branch Listing</a>
        </div>
      </div>
      <div class="panel-body">


        <div class="container-fluid viewTableLayout">
          <div class="row">
            <div class="col-sm-4">Branch Name:</div>
            <div class="col-sm-8"><?php echo $branch->branchName;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Contact Person:</div>
            <div class="col-sm-8"><?php echo $branch->branchContactName;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Contact Telephone:</div>
            <div class="col-sm-8"><?php echo $branch->branchContactTel;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Contact E-Mail:</div>
            <div class="col-sm-8"><?php echo $branch->branchContactEmail;?></div>
          </div>                              
          <div class="row">
            <div class="col-sm-4">Created:</div>
            <div class="col-sm-8"><?php echo $branch->created;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Created By:</div>
            <div class="col-sm-8"><?php echo $branch->createdbyuser;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Last Updated:</div>
            <div class="col-sm-8"><?php echo $branch->updatedbyuser." on ".$branch->lastUpdated;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Current Status:</div>
            <?php if($branch->status == true):?>
                <div class="col-sm-8">Active</div>
          <?php else:?>
                <div class="col-sm-8">In-Active</div>
          <?php endif;?>

          </div> 
        </div>

    </div>
  </div>
</div>




