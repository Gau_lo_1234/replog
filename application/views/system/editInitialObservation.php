<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo lang('systemEditInitialObservationHeading');?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url('SystemAdmin/initialObservationListing')?>">Return To List</a>
        </div>
      </div>
      <div class="panel-body">
        <?php if(isset($message)):?>
        <div id="infoMessage"><?php echo $message;?></div>
      <?php endif;?>
        <!-- Table -->


           <form method="post" action="<?php echo base_url('SystemAdmin/editInitialObservation')?>">

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="observation">Initial Observation:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="observation" name="observation"  value="<?php echo $result->observation;?>" placeholder="Initial Observation Description"  size="50" required/>
                  </div>

                </div>


                <div class="row">

                  
                  <div class="col-sm-3 inputStyle" >
                    <input type="hidden" name="id" value="<?php  echo $result->observationID;?>"/>
                     <button class="btn btn-primary" id="submit" name="submit" type="submit">Submit</button>
                  </div>
                  <div class="col-sm-9" >
                    
                  </div>
                </div>


            </form>
    </div>
  </div>
</div>