<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo lang('systemEditrepairCategoryHeading');?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url('SystemAdmin/repairCategoryListing')?>">Return To List</a>
        </div>
      </div>
      <div class="panel-body">
        <?php if(isset($message)):?>
        <div id="infoMessage"><?php echo $message;?></div>
      <?php endif;?>
        <!-- Table -->

           <form method="post" action="<?php echo base_url('SystemAdmin/editrepairCategory')?>">

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="categoryName">Category:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="categoryName" name="categoryName"  value="<?php echo $result->categoryName;?>" placeholder="Category"  size="50" required/>
                  </div>

                </div>

                <div class="row">

                  
                  <div class="col-sm-3 inputStyle" >
                    <input type="hidden" name="id" value="<?php echo $result->categoryID;?>"/>
                     <button class="btn btn-primary" id="submit" name="submit" type="submit">Submit</button>
                  </div>
                  <div class="col-sm-9" >
                    
                  </div>
                </div>


            </form>
    </div>
  </div>
</div>