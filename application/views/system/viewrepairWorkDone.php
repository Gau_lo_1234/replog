<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo lang('systemViewrepairWorkDoneHeading');?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url('SystemAdmin/repairWorkDoneListing')?>">Return To Repair Work Done Listing</a>
        </div>
      </div>
      <div class="panel-body">


        <div class="container-fluid viewTableLayout">


          <div class="row">
            <div class="col-sm-4">Work Done:</div>
            <div class="col-sm-8"><?php echo $result->workDoneName;?></div>
          </div>

          <div class="row">
            <div class="col-sm-4">Work Done Cost:</div>
            <div class="col-sm-8"><?php echo "R ".number_format($result->workDoneCost,2);?></div>
          </div>          


          <div class="row">
            <div class="col-sm-4">Linked To Category:</div>
            <div class="col-sm-8"><?php echo $result->categoryName;?></div>
          </div>

          <div class="row">
            <div class="col-sm-4">Linked To Fault:</div>
            <div class="col-sm-8"><?php echo $result->faultName;?></div>
          </div>

          <div class="row">
            <div class="col-sm-4">Created:</div>
            <div class="col-sm-8"><?php echo $result->created;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Created By:</div>
            <div class="col-sm-8"><?php echo $result->createdByuser;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Last Updated:</div>
            <div class="col-sm-8"><?php echo $result->lastUpdated;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Current Status:</div>
            <?php if($result->status == true):?>
                <div class="col-sm-8">Active</div>
          <?php else:?>
                <div class="col-sm-8">In-Active</div>
          <?php endif;?>

          </div>          
        </div>

    </div>
  </div>
</div>




