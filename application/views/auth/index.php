<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1><?php 
					echo lang('index_heading');?>
				</h1>
				<div class="menuReturn btn btn-default">
					<a href="<?php echo base_url('dashboard')?>">Return To Dashboard</a>
				</div>
			</div>
			<div class="panel-body">
				<p><?php echo lang('index_subheading');?></p>
				<div id="infoMessage"><?php echo $message;?></div>
			</div>

			  <!-- Table -->
				  <table class="table">
						
							<tr>
								<?php if($this->ion_auth->is_superadmin()):?>
								<th>Company Name</th>
								<th>Branch Name</th>
							<?php else:?>
								<th>Branch Name</th>
								<?php endif;?>
								<th><?php echo lang('index_fname_th');?></th>
								<th><?php echo lang('index_lname_th');?></th>
								<th><?php echo lang('index_email_th');?></th>
								<th><?php echo lang('index_groups_th');?></th>
								<th><?php echo lang('index_status_th');?></th>
								<th><?php echo lang('index_action_th');?></th>
							</tr>
							<?php foreach ($users as $user):?>
								<tr>
								<?php if($this->ion_auth->is_superadmin()):?>
						            <td><?php echo htmlspecialchars($user->companyName,ENT_QUOTES,'UTF-8');?></td>
						            <td><?php echo htmlspecialchars($user->branchName,ENT_QUOTES,'UTF-8');?></td>
						        <?php else:?>
						         <td><?php echo htmlspecialchars($user->branchName,ENT_QUOTES,'UTF-8');?></td>
								<?php endif;?>									
									
						            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
						            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
						            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
									<td>
										<?php foreach ($user->groups as $group):?>
											<?php echo $group->name;//anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
						                <?php endforeach?>
									</td>
									<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?></td>
									<td><?php 
									echo anchor("auth/view_user/".$user->id,'View');
									echo "&nbsp;|&nbsp";
										echo anchor("auth/edit_user/".$user->id, 'Edit') ;?>
									</td>
								</tr>
							<?php endforeach;?>
						
				  </table>
				  <p>&nbsp;<?php echo anchor('auth/create_user', lang('index_create_user_link'))?>  <?php //echo anchor('auth/create_group', lang('index_create_group_link'))?></p>
		</div>
	</div>
</div>









