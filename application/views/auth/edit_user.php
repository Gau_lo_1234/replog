<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo lang('edit_user_heading');?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url('auth/index')?>">Return To User Listing</a>
        </div>
      </div>
      <div class="panel-body">
        <p><?php echo lang('edit_user_subheading');?></p>
        <div id="infoMessage"><?php echo $message;?></div>


<?php echo form_open(uri_string());?>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="first_name">Company Profile:</label>
                    
                  </div>
                  <div class="col-sm-9" >

                    <?php if($this->ion_auth->is_superadmin()):?>
                    <select name="companyID" id="companyID">
                      <option value="<?php echo $user->companyID?>"><?php echo $user->companyName;?></option>
                      <option value=''>Please select Users Company Profile</option>

                      <?php foreach($compProfiles as $Company):?>
                      <option value="<?php echo $Company->companyID;?>"><?php echo $Company->companyName?></option>
                    <?php endforeach;?>
                    </select>

                      <?php else:?>
                      <input type="hidden" name="companyID" value="<?php echo $this->session->userdata('companyID');?>"/>
                      <input type="text" readonly value="<?php echo $this->session->userdata('companyName')?>"/>
                      <?php endif;?>
                    
                  </div>

                </div>


                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="first_name">Branch Profile:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <select name="branchID" id="branchID">

                      
                      <?php if($this->ion_auth->is_superadmin()):?>
                      <option value='<?php echo $user->branchID?>'><?php echo $user->branchName;?></option>
                      <?php else:?>
                        <?php foreach($branchProfiles as $branch):?>
                        <?php if($branch->companyID = $this->session->userdata('companyID')):?>
                            <option value='<?php echo $branch->branchID?>'><?php echo $branch->branchName;?></option>
                      <?php endif;?>
                      <?php endforeach;?>

                      <?php endif;?>
                    </select>
                  </div>

                </div>



                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="first_name">This User will be a:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <select name="groupType" id="groupType">
                      <option value='<?php echo $user->userGroupID;?>'><?php echo $user->groupName;?></option>
                      <option value=''>Please select Group this user will belong to</option>
                      <?php if($this->ion_auth->is_superadmin()):?>
                      <option value='1'>Super User (Can Control Everything)</option>
                      <option value='2'>Admin (Can Control Company Functions For Their Company)</option>
                      <option value="3">Member (Can Interact within the system)</option>
                      <?php else:?>
                      <option value='2'>Admin (Can Control Company Functions For Their Company)</option>
                      <option value="3">Member (Can Interact within the system)</option>
                      <?php endif;?>
                    </select>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="first_name">First Name:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="first_name" name="first_name" placeholder="First Name"  value="<?php echo $first_name['value'];?>" size="50" required/>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="last_name">Last Name:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="last_name" name="last_name" placeholder="Last Name" value="<?php echo $last_name['value']?>" size="50"/>
                  </div>
                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="email">Email:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="email" value="<?php echo $email['value']?>" name="email" placeholder="E-mail" size="100" required/>
                  </div>
                </div>


                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="phone">Phone:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="phone" name="phone" placeholder="Phone" value="<?php echo $phone['value'];?>" size="50" />
                  </div>
                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="password">Password:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="password" name="password" placeholder="Password" size="50"/>
                  </div>
                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="password_confirm">Password Confirm:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="password_confirm" name="password_confirm"  size="50" placeholder="Confirm Password" />
                  </div>
                </div>

                
                <?php if ($this->ion_auth->is_admin()): ?>

                          <div class="row formRowSpacing">

                  <div class="col-sm-3" >
                     <h3><?php echo lang('edit_user_groups_heading');?></h3>
                    
                  </div>
                   <div class="col-sm-9" >
                    <?php foreach ($groups as $group):?>
                        <label class="checkbox">
                        <?php
                            $gID=$group['id'];
                            $checked = null;
                            $item = null;
                            foreach($currentGroups as $grp) {
                                if ($gID == $grp->id) {
                                    $checked= ' checked="checked"';
                                break;
                                }
                            }
                        ?>
                        <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
                        <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
                        </label>
                    <?php endforeach?> </div>                   



                          </div>

                <?php endif ?>


      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?>


                <div class="row">
                  <div class="col-sm-3 inputStyle" >
                     <button class="btn btn-primary" id="submit" name="submit" type="submit"><?php echo lang('edit_user_submit_btn')?></button>
                  </div>
                  <div class="col-sm-9" >
                    
                  </div>
                </div>


<?php echo form_close();?>


        <!-- Table -->



    </div>
  </div>
</div>
<script>
//instantiate the branch list array
 branches = <?php echo json_encode($branchProfiles);?>;
  //initiate document ready function
  $(document).ready(function(){

    $('#companyID').change(function(){

      sel_company = $(this).val();

      //populate branch listing based on selected company id
      sel_content = "<option value=''>Please Select Users Branch Profile</option>";

       $.each(branches,function(key,val){
        //alert('branch id:'+val['branchID']+' - Branch Name: '+val['branchName']);

         if(val['companyID'] == sel_company){
            sel_content += "<option value='"+val.branchID+"'>"+val.branchName+"</option>";
         }
       
       });

      $('#branchID').empty();
      $('#branchID').append(sel_content);

    });


  });
</script>