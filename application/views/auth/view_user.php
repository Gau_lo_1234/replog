<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo "View User Details";?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url('Auth/Index')?>">Return To User Listing</a>
        </div>
      </div>
      <div class="panel-body">


        <div class="container-fluid viewTableLayout">
          <div class="row">
            <div class="col-sm-4">First Name:</div>
            <div class="col-sm-8"><?php echo $users->first_name;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Last Name:</div>
            <div class="col-sm-8"><?php echo $users->last_name;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Telephone:</div>
            <div class="col-sm-8"><?php echo $users->phone;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">E-Mail:</div>
            <div class="col-sm-8"><?php echo $users->email;?></div>
          </div>                              
          <div class="row">
            <div class="col-sm-4">Created:</div>
            <div class="col-sm-8"><?php echo date("Y-m-d H:i:s",$users->created_on);?></div>
          </div>

          <div class="row">
            <div class="col-sm-4">Current Status:</div>
            <?php if($users->active == true):?>
                <div class="col-sm-8">Active</div>
          <?php else:?>
                <div class="col-sm-8">In-Active</div>
          <?php endif;?>

          </div> 
        </div>

    </div>
  </div>
</div>




