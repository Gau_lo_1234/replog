 
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Replog V2 User Admin</title>
 

 
<!--[if IE]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lte IE 7]>
  <script src="js/IE8.js" type="text/javascript"></script><![endif]-->
<!--[if lt IE 7]>
 
  <link rel="stylesheet" type="text/css" media="all" href="css/ie6.css"/><![endif]-->
<!--  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css')?>"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.bootstrap.min.css')?>"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/buttons.dataTables.min.css')?>"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.css')?>" /> 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-ui.css')?>"/>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/default.css')?>"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/forms.css')?>"/>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datepicker.css')?>"/>

	<!-- All Script Tags Go Here-->
	<script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-ui.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/jszip.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/pdfmake.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/vfs_fonts.js')?>"></script>
	<script src="<?php echo base_url('assets/js/dataTables.buttons.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/buttons.flash.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/buttons.html5.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/buttons.print.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/dataTables.bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.js')?>"></script>
  
</head>
 
<body>

<div class="container-fluid loginForm">
  <div class="row" style="padding-top: 100px;">
    <div class="col-sm-6 col-sm-offset-3 text-center">
      <h1><strong>MAG</strong>TOUCH</h1>
      <form  style="margin:auto;" method="post" action="<?php echo base_url('auth/login')?>">
      <h4>Sign In to your Account</h4>
      <?php echo $message;?>
      <br/>
      <br/>
      <div class="row">
        <div class="col-sm-4" >
          <label for="identity">Username/Email:</label>
          
        </div>
        <div class="col-sm-8 inputStyle" >
          <input type="text" id="identity" name="identity" required placeholder="Username/Email Address"/>
        </div>
      </div>
      <br/>
      <div class="row">
        <div class="col-sm-4">
          <label for="password">Password:</label>
        </div>
        <div class="col-sm-8 inputStyle" >
          <input type="password" id="password" name="password" required placeholder="Password"/>
        </div>
      </div> 
      <div class="row">
        <br/>
        <div class="col-sm-4 loginLabel"></div>
       <div class="col-sm-8 inputStyle" >
          <button  type="submit" class="btn btn-primary" name="submit" id="submit">Login</button>
        </div>
      </div> 
      <div class="row">
        <div class="col-sm-4">
          <label for="rememberMe">Remember Me:</label>
        </div>
        <div class="col-sm-8">
          <input type="checkbox" id="rememberMe" name="rememberMe"/>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          &nbsp<a href="<?php echo base_url('Auth/reset_password')?>">FORGOTTEN PASSWORD</a>
        </div>
      </div>               
      </form>
    </div>
  </div>
</div>





</body>
</html>