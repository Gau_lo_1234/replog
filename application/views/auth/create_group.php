<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo lang('create_group_heading');?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url('auth/index')?>">Return To User Listing</a>
        </div>
      </div>
      <div class="panel-body">
        <p><?php echo lang('create_group_subheading');?></p>
        <div id="infoMessage"><?php echo $message;?></div>

        <!-- Table -->


           <form method="post" action="<?php echo base_url('auth/create_group')?>">


                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="group_name">Group Name:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="group_name" name="group_name" placeholder="Group Name"  size="50" required/>

                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="description">Description:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="description" name="description" placeholder="Description" size="100" required/>

                  </div>
              </div>
                <div class="row">
                  <div class="col-sm-3 inputStyle" >
					             <button class="btn btn-primary" id="submit" name="submit" type="submit"><?php echo lang('create_group_submit_btn')?></button>
                  </div>
                  <div class="col-sm-9" >
                    
                  </div>
                </div>

            </form>
    </div>
  </div>
</div>