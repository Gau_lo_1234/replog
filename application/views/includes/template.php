 
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title><?php echo $title?></title>
 

 
<!--[if IE]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lte IE 7]>
  <script src="js/IE8.js" type="text/javascript"></script><![endif]-->
<!--[if lt IE 7]>
 
  <link rel="stylesheet" type="text/css" media="all" href="css/ie6.css"/><![endif]-->
<!--  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css')?>"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.bootstrap.min.css')?>"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/buttons.dataTables.min.css')?>"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.css')?>" /> 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-ui.css')?>"/>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/default.css')?>"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/forms.css')?>"/> 
   <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datepicker.css')?>"/>
	<!-- All Script Tags Go Here-->
	<script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-ui.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/jszip.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/pdfmake.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/vfs_fonts.js')?>"></script>
	<script src="<?php echo base_url('assets/js/dataTables.buttons.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/buttons.flash.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/buttons.html5.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/buttons.print.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/dataTables.bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.js')?>"></script>
  
</head>
 
<body>
		<?php $this->load->view('includes/navigation')?>
<div class="container-fluid" style="padding-bottom:25px;">

<div class="row">
	<?php if($this->ion_auth->logged_in()):?>
	<div class="col-sm-3">
		<div class="row marginBottom">
		<form method="post" action="<?php echo base_url('dashboard/search')?>" class="navbar-form navbar-left">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
			<button type="submit" class="btn btn-default dashboardSearch">Submit</button>
		</form>
		<br/>

		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
				  <!-- Default panel contents -->
				  <div class="panel-heading">Notifications will appear here</div>
				  <div class="panel-body">

				  </div>
				</div>
				
			</div>
		</div>
	</div>
	<div class="col-sm-9">
		<div class="row">

		</div>
		<div class="row">
			<div class="col-sm-12">
				<?php $this->load->view($content)?>
			</div>
		</div>
	</div>
	<?php else:?>
	<div class="col-sm-12">
		<div class="row">

		</div>
		<div class="row">
			<div class="col-sm-12">
				<?php $this->load->view($content)?>
			</div>
		</div>
	</div>
	<?php endif;?>

</div>




</div>
<!--  <div class="navbar navbar-default navbar-fixed-bottom" style="background-color:#291676;">-->
<?php $this->load->view('includes/footer')?>
</body>
</html>
