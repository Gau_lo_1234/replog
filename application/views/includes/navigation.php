<nav class="navbar navbar-default">
<div class="container-fluid">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" style="color:white;" href="#"><strong style="font-size:1.2em;">MAG</strong>TOUCH</a>
	</div>



<?php if($this->ion_auth->logged_in()):?>
	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav">
<!-- 			<li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li> -->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="#">Book In</a></li>
					<li><a href="#">Wait Collection</a></li>
				</ul>
			</li>

			<?php if($this->ion_auth->is_superadmin()):?>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin <span class="caret"></span></a>
				<ul class="dropdown-menu">
					
					<li><a href="#">Sell Unit</a></li>
					<li><a href="<?php echo base_url('StockAdmin/productTypeListing')?>">Product Types</a></li>
					<li><a href="#">Repairs Work</a></li>
					<li><a href="<?php echo base_url('SystemAdmin/repairCategoryListing')?>">Category's</a></li>
					<li><a href="<?php echo base_url('SystemAdmin/repairFaultListing')?>">Fault's</a></li>
					<li><a href="<?php echo base_url('SystemAdmin/repairWorkDoneListing')?>">Work Done</a></li><!-- 
					<li role="separator" class="divider"></li> -->
					<li><a href="<?php echo base_url('SystemAdmin/repairCauseListing')?>">Cause</a></li>
					<li><a href="<?php echo base_url('SystemAdmin/initObservationsListing')?>">Initial Observations</a></li>
				</ul>
			</li>

		<!-- 	<li><a href="#">Link</a></li> -->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Stock Management<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="#">Stock</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="#">Add Baton Stock</a></li>
					<li><a href="#">Add Other Stock</a></li>
					<li><a href="#">Add Old Stock</a></li>
					<li><a href="#">Book Back Stock</a></li>
				</ul>
			</li>
			<?php endif;?>	
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span></a>
				<ul class="dropdown-menu">
			<?php if($this->ion_auth->is_superadmin()):?>					
					<li><a href="#">Product Report</a></li>
					<li><a href="#">Stock Report</a></li>
			<?php endif;?>
					<li><a href="#">Work Done Report</a></li>
				</ul>
			</li>
			<li><a href="#">Repairs</a></li>
			<?php if($this->ion_auth->is_superadmin() || $this->ion_auth->is_admin()):?>
						<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Clients <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo base_url('ClientAdmin/accountTypeListing')?>">Client Account Types</a></li>
					<li><a href="<?php echo base_url('ClientAdmin/index')?>">Client Profiles</a></li>
				</ul>
			</li>
			
			<li><a href="<?php echo base_url('auth/index')?>">Users</a></li>

			<li><a href="<?php echo base_url('CompanyProfileAdmin/compProfiles')?>">Company Profiles</a></li>
			<?php endif;?>
		</ul>
<!-- 		<form class="navbar-form navbar-left">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
			<button type="submit" class="btn btn-default">Submit</button>
		</form> -->
		<ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo ucfirst($this->session->userdata('username'));?>&nbsp;<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="#">My Profile</a></li>
					<li><a href="#">Create User Group</a></li>
					<li><a href="<?php echo base_url('auth/Logout')?>">Logout</a></li>
				</ul>
			</li>
		</ul>
	</div><!-- /.navbar-collapse -->

<?php endif;?>
</div><!-- /.container-fluid -->
</nav>