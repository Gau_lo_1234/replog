<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo lang('stockCreateProductTypeHeading');?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url('StockAdmin/productTypeListing')?>">Return To List</a>
        </div>
      </div>
      <div class="panel-body">
        <?php if(isset($message)):?>
        <div id="infoMessage"><?php echo $message;?></div>
      <?php endif;?>
        <!-- Table -->

           <form method="post" action="<?php echo base_url('StockAdmin/createProductType')?>">

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="productTypeName">Product Type Name:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="productTypeName" name="productTypeName"  placeholder="Product Type Name"  size="50" required/>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="productType">Product Type:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="productType" name="productType"  placeholder="Product Type"  size="25"/>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="productTypeAbbr">Product Type Abbr:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="productTypeAbbr" name="productTypeAbbr"  placeholder="Product Type Abbr"  size="25"/>
                  </div>

                </div>


                <div class="row">

                  
                  <div class="col-sm-3 inputStyle" >
                     <button class="btn btn-primary" id="submit" name="submit" type="submit">Submit</button>
                  </div>
                  <div class="col-sm-9" >
                    
                  </div>
                </div>


            </form>
    </div>
  </div>
</div>