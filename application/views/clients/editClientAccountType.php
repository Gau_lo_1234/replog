<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo lang('editClientAccountTypeHeading');?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url('ClientAdmin/accountTypeListing')?>">Return To List</a>
        </div>
      </div>
      <div class="panel-body">
        <?php if(isset($message)):?>
        <div id="infoMessage"><?php echo $message;?></div>
         <?php echo form_validation();?>
      <?php endif;?>
        <!-- Table -->


           <form method="post" action="<?php echo base_url('ClientAdmin/editClientAccountType/'.$result->accountTypeID)?>">


            <div class="row formrowSpacing">

                <div class="col-sm-3">
                  <label for="companyID">Company Name:</label>
                </div>

                <div class="col-sm-9" >
                  <!-- If user is super admin we need to select distributor profile to add client profile to -->
                  <?php if($this->ion_auth->is_superadmin()):?>
                  <select name="companyID">
                    <option value='<?php echo $result->companyID;?>'><?php echo $result->companyName;?></option>
                      <option value=''>Please Select Company Profile</option>
                      <?php foreach($companies as $comp):?>
                      <?php if($comp->companyID != $result->companyID):?>
                        <option value='<?php echo $comp->companyID?>'><?php echo $comp->companyName;?></option>
                      <?php endif;?>
                    <?php endforeach;?>
                  </select>
                  <?php else:?>
                  <!-- show fields relating to users company profile -->
                  <input type="hidden" name="companyID" value="<?php echo $result->companyID;?>"/>           
                  <input type="text" size="100" readonly value="<?php echo $result->companyName;?>"/>
                  <?php endif;?>
                </div>
              </div>


                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="accountTypeName">Account Type Name:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="accountTypeName" name="accountTypeName" value="<?php echo $result->accountTypeName;?>" placeholder="Account Type Name"  size="50" required/>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="description">Description:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="description" name="description" value="<?php echo $result->description;?>" placeholder="Description"  size="100"/>
                  </div>

                </div>


                <div class="row">
                  <input type="hidden" name="id" value="<?php echo $result->accountTypeID;?>"/>
                  <div class="col-sm-3 inputStyle" >
                     <button class="btn btn-primary" id="submit" name="submit" type="submit">Submit</button>
                  </div>
                  <div class="col-sm-9" >
                    
                  </div>
                </div>


            </form>
    </div>
  </div>
</div>