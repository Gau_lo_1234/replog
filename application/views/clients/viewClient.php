<style>
textarea {
    resize: none;
}
</style>

<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo lang('viewClientHeading');?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url('ClientAdmin/Index')?>">Return To Client Listing</a>
        </div>
      </div>
      <div class="panel-body">

        <div class="container-fluid viewTableLayout">
          <div class="row">
            <div class="col-sm-4">Company Name:</div>
            <div class="col-sm-8"><?php echo $client->companyName;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Account Type:</div>
            <div class="col-sm-8"><?php echo $client->accountTypeName;?></div>
          </div>                    
          <div class="row">
            <div class="col-sm-4">Client Name:</div>
            <div class="col-sm-8"><?php echo $client->clientName;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Client Company Number:</div>
            <div class="col-sm-8"><?php echo $client->clientCompanyNumber;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Client Vat Number:</div>
            <div class="col-sm-8"><?php echo $client->clientVatNumber;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">contact Person:</div>
            <div class="col-sm-8"><?php echo $client->contactPerson;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Client Telephone:</div>
            <div class="col-sm-8"><?php echo $client->clientTelephone;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Client E-Mail:</div>
            <div class="col-sm-8"><?php echo $client->clientEmail;?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Client Physical Address:</div>
            <div class="col-sm-8"><textarea readonly cols="100" rows="5"><?php echo $client->clientPhysicalAddress;?></textarea></div>
          </div> 
          <div class="row">
            <div class="col-sm-4">Client Postal Address:</div>
            <div class="col-sm-8"><textarea readonly cols="100" rows="5"><?php echo $client->clientPostalAddress;?></textarea></div>
          </div>                     
          <div class="row">
            <div class="col-sm-4">Created:</div>
            <div class="col-sm-8"><?php echo date("Y-m-d H:i:s",strtotime($client->created));?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Last Updated:</div>
            <div class="col-sm-8"><?php echo (!is_null($client->lastUpdated))?date("Y-m-d H:i:s",strtotime($client->lastUpdated)):"Never Updated";?></div>
          </div>
          <div class="row">
            <div class="col-sm-4">Current Status:</div>
            <?php if($client->status == true):?>
                <div class="col-sm-8">Active</div>
          <?php else:?>
                <div class="col-sm-8">In-Active</div>
          <?php endif;?>

          </div> 
        </div>

    </div>
  </div>
</div>




