<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1><?php echo lang('clientListingHeading');?></h1>

				<div class="menuReturn btn btn-default">
					<a href="<?php echo base_url('dashboard')?>">Return To Dashboard</a>
				</div>
				<div class="menuReturnLeft btn btn-default">
					<a href="<?php echo base_url('ClientAdmin/createClient')?>">Create New</a>
				</div>
			</div>
			<div class="panel-body">
				<?php if($show_inactive == true):?>
					<a href="<?php echo base_url('ClientAdmin/index/0')?>">Hide In-Active Account Types</a>
				<?php else:?>
					<a href="<?php echo base_url('ClientAdmin/index/1')?>">Show In-Active Account Types</a>
				<?php endif;?>
				
				<?php if(isset($message)):?>
				<div id="infoMessage"><?php echo $message;?></div>
			<?php endif;?>
			</div>
			  <!-- Table -->
				  <table class="table">
						
							<tr>
								<th>Client Name</th>
								<th>Telephone</th>
								<th>E-Mail</th>
								<th>Created</th>
								<th>Current Status</th>
								<th>Actions</th>
							</tr>
							<?php if(!empty($results)):?>
							<?php foreach ($results as $result):?>
								<tr>
									<td><?php echo $result->clientName;?></td>
									<td><?php echo $result->clientTelephone;?></td>
									<td><?php echo $result->clientEmail;?></td>									
									<td><?php echo date('Y-m-d H:i:s',strtotime($result->created));?></td>
									<td>
										<?php if($result->status == true):?>
										Active
										<?php else:?>
										In-Active
										<?php endif;?>
										

									</td>
									<td>
										<!-- list actions available -->
										<a id="view" name="view" href="<?php echo base_url('ClientAdmin/viewClient/'.$result->clientID)?>">View</a>
										&nbsp;&nbsp;|&nbsp;&nbsp;

										<a id="edit" name="edit" href="<?php echo base_url('ClientAdmin/editClient/'.$result->clientID)?>">Edit</a>
										&nbsp;&nbsp;|&nbsp;&nbsp;
										<?php if($result->status == true):?>
											<a id="status" name="status" href="<?php echo base_url('ClientAdmin/changeClientStatus/'.$result->clientID.'/0')?>">Disable</a>
										<?php else:?>
											<a id="status" name="status" href="<?php echo base_url('ClientAdmin/changeClientStatus/'.$result->clientID.'/1')?>">Enable</a>
										<?php endif;?>
									</td>
								</tr>
							<?php endforeach;?>
							<?php else:?>
								<tr>
									<td colspan="6">
										<h3>No Records Found</h3>
									</td>
								</tr>
						<?php endif;?>
				  </table>
				  
		</div>
	</div>
</div>









