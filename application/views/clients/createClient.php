<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h1><?php echo lang('createClientHeading');?></h1>
        <div class="menuReturn btn btn-default">
          <a href="<?php echo base_url('ClientAdmin/index')?>">Return To List</a>
        </div>
      </div>
      <div class="panel-body">
        <?php if(isset($message)):?>
        <div id="infoMessage"><?php echo $message;?></div>
       <?php echo validation_errors(); ?>
      <?php endif;?>
        <!-- Table -->


           <form method="post" action="<?php echo base_url('ClientAdmin/CreateClient')?>">


            <div class="row formrowSpacing">

                <div class="col-sm-3">
                  <label for="companyID">Company Name:</label>
                </div>

                <div class="col-sm-9" >
                  <!-- If user is super admin we need to select distributor profile to add client profile to -->
                  <?php if($this->ion_auth->is_superadmin()):?>
                  <select name="companyID" id="companyID">
                      <option value=''>Please Select Company Profile</option>
                      <?php foreach($companies as $comp):?>
                        <option value='<?php echo $comp->companyID?>'><?php echo $comp->companyName;?></option>
                    <?php endforeach;?>
                  </select>
                  <?php else:?>
                  <!-- show fields relating to users company profile -->
                  <input type="hidden" name="companyID" value="<?php echo $this->session->userdata('companyID')?>"/>           
                  <input type="text" size="100" readonly value="<?php echo $this->session->userdata('companyName');?>"/>
                  <?php endif;?>
                </div>
              </div>


              <?php if($this->ion_auth->is_superadmin()):?>
              <div class="row formrowSpacing" id="accountTypePanel" style="display:none;">
              <?php else:?>
              <div class="row formrowSpacing" id="accountTypePanel">
            <?php endif;?>
                <div class="col-sm-3">
                  <label for="accountTypeID">Client Account Type:</label>
                </div>

                <div class="col-sm-9" >
                  <!-- If user is super admin we need to select distributor profile to add client profile to -->
                  <select name="accountTypeID" id="accountTypeID">
                    <?php echo "<option value=''>Please Select Account Type Profile</option>";?>
                    <?php foreach($accountTypes as $act):?>
                      <?php if($act->companyID == $this->session->userdata('companyID')):?>
                        <option value="<?php echo $act->accountTypeID;?>"><?php echo $act->accountTypeName;?></option>
                      <?php endif;?>
                    <?php endforeach;?>                    
                  </select>
                </div>
              </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="cientName">Client Name:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="clientName" name="clientName"  placeholder="Client Name"  size="50" required/>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="clientCompanyNumber">Company Number:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="clientCompanyNumber" name="clientCompanyNumber"  placeholder="Company Number"  size="50"/>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="clientVatNumber">Company Vat Number:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="clientVatNumber" name="clientVatNumber"  placeholder="Company Vat Number"  size="50"/>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="contactPerson">Contact Person:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="contactPerson" name="contactPerson"  placeholder="Contact Person"  size="100"/>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="clientTelephone">Contact Telephone:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="clientTelephone" name="clientTelephone"  placeholder="Telephone"  size="100"/>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="clientEmail">Contact E-Mail:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <input type="text" id="clientEmail" name="clientEmail"  placeholder="E-Mail"  size="100"/>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="clientPhysicalAddress">Physical Address:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <textarea rows="5" cols="100" id="clientPhysicalAddress" name="clientPhysicalAddress"  placeholder="Physical Address"></textarea>
                  </div>

                </div>

                <div class="row formRowSpacing">
                  <div class="col-sm-3" >
                     <label for="clientPostalAddress">Postal Address:</label>
                    
                  </div>
                  <div class="col-sm-9" >
                    <textarea rows="5" cols="100" id="clientPostalAddress" name="clientPostalAddress"  placeholder="Postal Address"></textarea>
                  </div>

                </div>

                <div class="row">

                  
                  <div class="col-sm-3 inputStyle" >
                     <button class="btn btn-primary" id="submit" name="submit" type="submit">Submit</button>
                  </div>
                  <div class="col-sm-9" >
                    
                  </div>
                </div>


            </form>
    </div>
  </div>
</div>
<script>
//instantiate the branch list array
 accountTypes = <?php echo json_encode($accountTypes);?>;
  //initiate document ready function
  $(document).ready(function(){

    $('#companyID').change(function(){

        companyID = $(this).val();

        if(companyID != ''){
          $('#accountTypePanel').css('display','block');

        }else{
          $('#accountTypePanel').css('display','none');
        }

        accountTypeOptions = "<option value=''>Please Select Account Type Profile</option>";

        $.each(accountTypes,function(key,val){
          if(val['companyID'] == companyID){
            accountTypeOptions += "<option value='"+val['accountTypeID']+"'>"+val['accountTypeName']+"</option>";
          }
        });

       $('#accountTypeID').empty();
       $('#accountTypeID').append(accountTypeOptions);

    });


  });
</script>