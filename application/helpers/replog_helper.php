<?php 

	function formatSelectList($selectName = null,$array = null,$selectID = null,$defaultString = null,$required = 0){

		if(is_null($selectName)){
			return false;
		}

		if($array && !empty($array)){

			//build opening select statement
				$response = "<select name='$selectName'";
				if(!is_null($selectID)){
					$response .= " id='$selectID'";
				};

				if($required == true){
					$response .= " required";
				}
				$response .= ">";



			//set default select opening statement
			if(!is_null($defaultString)){
				$response .= "<option val='' default>$defaultString</option>";
			}


			foreach($array as $key=>$val){
				$response .= "<option value='$key'>$val</option>";
			}
			$response .= "</select>";


			return $response;
		}else{
			return "Invalid Formatting";
		}
	}


	function dump($array = null){
		echo "<pre>",var_dump($array),"</pre>";
	}

?>