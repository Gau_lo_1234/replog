<?php


//$lang[] = '';
$lang['replog_app_title'] = 'Replog Systems';
$lang['replog_dashboard'] = 'Dashboard Interface';


$lang['clientsAccountTypeListing'] = 'Client Account Type Listing...';
$lang['clientAccountTypeListingHeading'] = "Account Type Listing...";
$lang['clientAccountTypeListingHeading'] = "Account Type Listing...";
$lang['createClientAccountTypeHeading'] = "Create New Client Account Type";
$lang['viewClientAccountType'] = "View Client Account Type Details";
$lang['editClientAccountTypeHeading'] = "Edit Client Account Type Details";
$lang['clientListingHeading'] = "Client Listing";
$lang['createClientHeading'] = "Create Client Profile";
$lang['viewClientHeading'] = "View Client Details";
$lang['editClientHeading'] = "Edit Client Details";

$lang['stockProductTypeListingHeading'] = 'Product Type Listing';
$lang['stockCreateProductTypeHeading'] = 'Create New Product Type';
$lang['stockViewProductTypeHeading'] =" View Product Type Details";
$lang['stockEditProductTypeHeading'] = "Edit Product Type Details";


$lang['systemInitialObservationsListingHeading'] = "Initial Observations Listing";
$lang['systemCreateInitialObservationHeading'] = "Create Initial Observation";
$lang['systemEditInitialObservationHeading'] = "Edit Initial Observations";
$lang['systemViewInitialObservationHeading'] = 'View Initial Repair Category Details';
$lang['systemrepairCategoryListingHeading'] = "Repair Category Listing";
$lang['systemCreaterepairCategoryHeading'] = "Create Repair Category";
$lang['systemEditrepairCategoryHeading'] = "Edit Repair Category Details";

$lang['systemrepairFaultListingHeading'] = "Repair Fault Listing";
$lang['systemCreaterepairFaultHeading'] = "Create New Repair Fault";
$lang['systemEditrepairFaultHeading'] = "Edit Repair Fault Item";
$lang['systemViewrepairFaultHeading'] = "View Repair Fault Details";

$lang['selectrepairCategoryHeading'] = "Select Repair Category Item To Link To:";
$lang['selectrepairFaultHeading'] = "Select Repair Fault Item To Link To:";
$lang['selectrepairWorkDoneHeading'] = "Select Repair Work Done Item To Link To:";


$lang['systemViewrepairWorkDoneHeading'] = "View Repair Work Done Details";
$lang['systemrepairWorkDoneListingHeading'] = "Repair Work Done Listing";
$lang['systemCreaterepairWorkDoneHeading'] = "Create Repair Work Done";
$lang['systemEditrepairWorkDoneHeading'] = "Edit Repair Work Done Item";


$lang['systemrepairCauseListingHeading'] = "Repair Cause Listing";
$lang['systemCreaterepairCauseHeading'] = "Create Repair Cause";


$lang['companyProfileListingHeading'] = "Company Profiles Listing";
$lang['systemCreateCompanyProfile'] = "Create Company Profile";
$lang['companyBranchListingHeading'] = "Company Branch Listing";
$lang['companyBranchCreateHeading'] = "Create Company Branch Listing";
$lang['systemViewCompanyProfileHeading'] = "View Company Profile";
$lang['systemViewCompanyBranchHeading'] = "View Company Branch Details";
$lang['systemcompanyBranchEditHeading'] = "Edit Company Branch Details";
 ?>