<?php 

if(!defined('BASEPATH'))exit('Direct script access not allowed');


class ClientAdminModel extends CI_Model{


	function __construct(){

		parent::__construct();
	}

	public function getClients($show_inactive = 0,$companyID = null){

		if($show_inactive){
			$sql = "select * from clients";
			if($companyID) $sql .= " where companyID = $companyID";

		}else{
			$sql = "select * from clients
					where status is true";
			if($companyID) $sql .= " and companyID = $companyID";			
		}

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->result();
		}else{
			return array();
		}
	}

	public function createClient($data = null){
		$insert = "insert into clients(
					companyID, accountTypeID,clientName,clientCompanyNumber,
					clientVatNumber,contactPerson,clientTelephone,clientEmail,
					clientPhysicalAddress,clientPostalAddress,created,createdBy
			       )values(
			       	{$data['companyID']},
			       	{$data['accountTypeID']},
			       	'{$data['clientName']}',
			       	'{$data['clientCompanyNumber']}',
			       	'{$data['clientVatNumber']}',
			       	'{$data['contactPerson']}',
			       	'{$data['clientTelephone']}',
			       	'{$data['clientEmail']}',
			       	'{$data['clientPhysicalAddress']}',
			       	'{$data['clientPostalAddress']}',
			       	'{$data['created']}',
			       	{$data['createdBy']})";

		if($this->db->query($insert)){
			return true;
		}else{
			return false;
		}
	}   

	public function getClient($clientID = null){
		$view = "select cl.*,
				c.companyName,
				cat.accountTypeName
				from clients cl
				left join Company c on c.companYID = cl.companyID
				left join clientAccountTypes cat on cat.accountTypeID = cl.accountTypeID
				where cl.clientID = $clientID";

		$client = $this->db->query($view);

		if($client && $client->num_rows() > 0){
			return $client->row();
		}else{
			return false;
		}
	}

	public function editClient($inputData = null){
		$update = "update clients set
				   companyID = {$inputData['companyID']},
				   accountTypeID = {$inputData['accountTypeID']},
				   clientName = '{$inputData['clientName']}',
				   clientCompanyNumber = '{$inputData['clientCompanyNumber']}',
				   clientVatNumber = '{$inputData['clientVatNumber']}',
				   contactPerson = '{$inputData['contactPerson']}',
				   clientTelephone = '{$inputData['clientTelephone']}',
				   clientEmail = '{$inputData['clientEmail']}',
				   clientPhysicalAddress = '{$inputData['clientPhysicalAddress']}',
				   clientPostalAddress = '{$inputData['clientPostalAddress']}',
				   lastUpdated = '{$inputData['lastUpdated']}',
				   lastUpdatedBy = {$inputData['lastUpdatedBy']}
				   where clientID = {$inputData['clientID']}";

		if($this->db->query($update)){
			return true;
		}else{
			return false;
		};

	}




	public function changeClientStatus($inputData = null){
		$sql = "update clients 
				set status = {$inputData['status']},
				lastUpdated = now() 
				where clientID = {$inputData['id']}";
		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}


	function getAccountTypes($show_inactive = 0,$companyID = 0){
		

		if($show_inactive == true){
			$sql = "select cat.*,c.companyName from clientAccountTypes cat
			left join Company as c on c.companyID = cat.companyID";
			if($companyID) $sql .= "where cat.companyID = $companyID";
		}else{

			$sql = "select cat.*,c.companyName from clientAccountTypes cat
			left join Company as c on c.companyID = cat.companyID
			 where cat.status is true";
			if($companyID) $sql .= " and cat.companyID = $companyID";
		}

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->result();
		}else{
			return array();
		}
	}

	function createClientAccountType($data = null){


		$sql = "insert into clientAccountTypes(
			accountTypeName,companyID,description,created,createdBy,lastUpdated)values(
			'{$data['accountTypeName']}',
			{$data['companyID']},
			'{$data['description']}',
			'{$data['created']}',
			{$data['createdBy']},
			'{$data['lastUpdated']}')";


		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	function getClientAccountType($id = null){
		$sql = "select cat.*,u.username as createdByuser, c.companyName from clientAccountTypes cat 
				left join users as u on u.id = cat.createdby
				left join Company as c on c.companyID = cat.companyID
				where cat.accountTypeID = $id";

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->row();
		}else{
			return array();
		}
	}


	function changeAccountTypeStatus($inputData = null){
		$sql = "update clientAccountTypes 
				set status = {$inputData['status']},
				lastUpdated = now() 
				where accountTypeID = {$inputData['id']}";
		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}


	function updateClientAccountType($id = null, $inputData = null){
		$sql = "update clientAccountTypes set 
				accountTypeName = '{$inputData['accountTypeName']}',
				companyID = {$inputData['companyID']},
				description = '{$inputData['description']}',
				lastUpdated = '{$inputData['lastUpdated']}'
				where accountTypeID = $id";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}
}


?>