<?php

if(!defined('BASEPATH'))exit('direct script access not allowed');

class StockAdminModel extends CI_Model{

	function __construct(){
		parent::__construct();

		if(!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	function productTypeListing($show_inactive = 0){
		if($show_inactive == true){
			$sql = "select * from productTypes";
		}else{
			$sql = "select * from productTypes where status = true";
		}

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->result();
		}else{
			return array();
		}
	}

	function getProductTypes($show_inactive = 0){

		if($show_inactive == true){
			$sql = "select * from productTypes";
		}else{
			$sql = "select * from productTypes where status is true";
		}

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->result();
		}else{
			return false;
		}
	}



	function createProductType($inputData = null){
		$sql = "insert into productTypes(
			productTypeName, productType,productTypeAbbr,created,createdBy,lastUpdated
			)values(
			'{$inputData['productTypeName']}',
			{$inputData['productType']},
			'{$inputData['productTypeAbbr']}',
			'{$inputData['created']}',
			{$inputData['createdBy']},
			'{$inputData['lastUpdated']}')";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	function changeProductTypeStatus($inputData = null){
		$sql = "update productTypes 
				set status = {$inputData['status']},
				lastUpdated = now() 
				where productTypeID = {$inputData['id']}";
		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}		
	}

	function getProductType($id = null){
		$sql = "select pt.*,u.username as createdByuser from productTypes pt 
				left join users as u on u.id = pt.createdby
				where pt.productTypeID = $id";

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->row();
		}else{
			return array();
		}
	}

	function EditProductType($id = null, $data = null){
		$sql = "update productTypes set 
				productTypeName = '{$data['productTypeName']}',
				productType = {$data['productType']},
				productTypeAbbr = '{$data['productTypeAbbr']}',
				lastUpdated = '{$data['lastUpdated']}'
				where productTypeID = $id";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}
}

?>