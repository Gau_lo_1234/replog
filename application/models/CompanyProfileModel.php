<?php

if(!defined('BASEPATH'))die("Direct script access not allowed");


class CompanyProfileModel extends CI_Model{

	function __construct(){
		parent::__construct();
	}


	function getProfiles($show_inactive = 0,$companyID = null){
		if($show_inactive){
			$sql = "select c.*,b.branchCount,
					u.username,
					ifnull(lu.username,'Not Updated') as updatedbyuser
					from Company c 
					left join (select count(*) branchCount, companyID from Branches group by companyID) as b on b.companyID = c.companyID
					left join users as u on u.id = c.createdBy
					left join users as lu on lu.id = c.lastUpdatedBy";
			$sql .= ($companyID)?" where c.companyID = $companyID":"";
		}else{
			$sql = "select c.*,b.branchCount,
					u.username,
					ifnull(lu.username,'Not Updated') as updatedbyuser
					from Company c 
					left join (select count(*) branchCount, companyID from Branches group by companyID) as b on b.companyID = c.companyID
					left join users as u on u.id = c.createdBy
					left join users as lu on lu.id = c.lastUpdatedBy
				 	where status = true";
			$sql .= ($companyID)?" and c.companyID = $companyID":"";
		}
		
		$query = $this->db->query($sql);

		if($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return array();
		}
	}

	public function getCompanyProfile($id = null){
		$sql = "select c.*,b.branchCount,
				u.username,
				ifnull(lu.username,'Not Updated') as updatedbyuser
				from Company c 
				left join (select count(*) branchCount, companyID from Branches group by companyID) as b on b.companyID = c.companyID
				left join users as u on u.id = c.createdBy
				left join users as lu on lu.id = c.lastUpdatedBy
				where c.companyID = $id";
		
		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->row();
		}else{
			return false;
		}
	}


	function createCompanyProfile($insertData){
		$sql = "insert into Company(
				companyName,companyContactName,companyContactTel,companyContactEmail,created,createdBy,lastUpdated
				)values(
				'{$insertData['companyName']}',
				'{$insertData['companyContactName']}',
				'{$insertData['companyContactTel']}',
				'{$insertData['companyContactEmail']}',
				'{$insertData['created']}',
				{$insertData['createdBy']},
				'{$insertData['lastUpdated']}')";

		if($this->db->query($sql)){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

	function editCompanyProfile($companyID = null,$updateData = null){
		$sql = "update Company set 
				companyName = '{$updateData['companyName']}',
				companyContactName = '{$updateData['companyContactName']}',
				companyContactTel = '{$updateData['companyContactTel']}',
				companyContactEmail = '{$updateData['companyContactEmail']}'
				where companyID = $companyID";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	function changeProfileStatus($id = null,$status = null){
		if($this->db->query("update Company set status = $status where companyID = $id")){
			return true;
		}else{
			return false;
		}
	}

	function createBranchProfile($insertData){
		$sql = "insert into Branches(
				companyID, branchName, branchContactName,branchContactTel,branchContactEmail,created,createdBy,lastUpdated
				)values(
				{$insertData['companyID']},
				'{$insertData['branchName']}',
				'{$insertData['branchContactName']}',
				'{$insertData['branchContactTel']}',
				'{$insertData['branchContactEmail']}',
				'{$insertData['created']}',
				 {$insertData['createdBy']},
				'{$insertData['lastUpdated']}')";

		if($this->db->query($sql)){
			return $this->db->insert_ID();
		}else{
			return false;
		}
	}

	function getCompanyBranches($id = null,$show_inactive = 0){
		if($show_inactive){
			$sql = "select b.*,
					c.companyName,
                    u.username,
                    ifnull(l.username,'Not Updated')as lastupdatedbyuser
					from Branches as b
					left join Company as c on c.companyID = b.companyID
                    left join users as u on u.id = b.createdBy
                    left join users as l on l.id = b.lastUpdatedBy
                    where b.companyID = $id";
		}else{
			$sql = "select b.*,
					c.companyName,
                    u.username,
                    ifnull(l.username,'Not Updated')as lastupdatedbyuser
					from Branches as b
					left join Company as c on c.companyID = b.companyID
                    left join users as u on u.id = b.createdBy
                    left join users as l on l.id = b.lastUpdatedBy
                    where b.companyID = $id
					and b.status = true";
		}
		
		$query = $this->db->query($sql);

		if($query && $query->num_rows() > 0){
			return $query->result();
		}else{
			return array();
		}		
	}

	function getCompanyBranchProfile($branchID = null){
		$sql = "select b.* ,
				c.companyName,
				u.username as createdbyuser,
				lu.username as updatedbyuser
				from Branches b
				left join Company c on c.companyID = b.companyID
				left join users as u on u.id = b.createdBy
				left join users as lu on lu.id = b.lastUpdatedBy
				where b.branchId = $branchID";

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->row();
		}else{
			return falsxe;
		}
	}

	function editBranchProfile($branchID = null, $uploadData = null){
		$sql = "update Branches set
				branchName = '{$uploadData['branchName']}',
				branchContactName = '{$uploadData['branchContactName']}',
				branchContactTel = '{$uploadData['branchContactTel']}',
				branchContactEmail = '{$uploadData['branchContactEmail']}',
				lastUpdated = '{$uploadData['lastUpdated']}',
				lastUpdatedBy = {$uploadData['lastUpdatedBy']}
				where branchID = {$uploadData['branchID']}";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	function getBranchSelect(){
		$sql = "select * from Branches where status is true";

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->result();
		}else{
			return array();
		}
	}
}

?>