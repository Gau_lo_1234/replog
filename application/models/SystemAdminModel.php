<?php

if(!defined('BASEPATH'))die('Direct script access not allowed');

class SystemAdminModel extends CI_Model{

	function __construct(){
		parent::__construct();
		if(!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}


	/*

		Initial Observation Section

	*/

	function getInitialObservations($show_inactive = 0){
		if($show_inactive == true){
			$sql = "select * from initObservations";
		}else{
			$sql = "select * from initObservations where status is true";
		}

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->result();
		}else{
			return array();
		}
	}

	function createInitialObservation($data){
		$sql = "insert into initObservations(
			observation,created,createdBy,lastUpdated
			)values(
			'{$data['observation']}',
			'{$data['created']}',
			{$data['createdBy']},
			'{$data['lastUpdated']}')";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	function editInitialObservation($id = null,$inputData = null){

		$sql = "update initObservations set
				observation = '{$inputData['observation']}',
				lastUpdated = '{$inputData['lastUpdated']}'
				where observationID = $id";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	function getInitialObservation($id = null){
		$sql = "select iO.*,u.username as createdByuser from initObservations iO 
				left join users as u on u.id = iO.createdby
				where iO.observationID = $id";

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->row();
		}else{
			return array();
		}
	}

	function changeinitObservationStatus($inputData){
		$sql = "update initObservations 
				set status = {$inputData['status']},
				lastUpdated = now() 
				where observationID = {$inputData['id']}";
		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}	
	}


	/*

		Repair Category Section

	*/

	function getrepairCategorys($show_inactive = 0){
		if($show_inactive == true){
			$sql = "select * from repairCategory";
		}else{
			$sql = "select * from repairCategory where status is true";
		}

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->result();
		}else{
			return array();
		}
	}

	function createrepairCategory($data){
		$sql = "insert into repairCategory(
			categoryName,created,createdBy,lastUpdated
			)values(
			'{$data['categoryName']}',
			'{$data['created']}',
			{$data['createdBy']},
			'{$data['lastUpdated']}')";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	function editrepairCategory($id = null,$inputData = null){

		$sql = "update repairCategory set
				CategoryName = '{$inputData['categoryName']}',
				lastUpdated = '{$inputData['lastUpdated']}'
				where categoryID = $id";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	function getrepairCategory($id = null){
		$sql = "select cat.*,u.username as createdByuser from repairCategory cat 
				left join users as u on u.id = cat.createdby
				where cat.categoryID = $id";

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->row();
		}else{
			return array();
		}
	}

	function changerepairCategoryStatus($inputData){
		$sql = "update repairCategory 
				set status = {$inputData['status']},
				lastUpdated = now() 
				where categoryID = {$inputData['id']}";
		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}	
	}

	function getCategorySelectList(){
		$sql = "select categoryID, categoryName from repairCategory where status is true";

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){

			return $result->result();
		}else{
			return array();
		}

	}

	function getrepairCategoryInactiveCount(){
		$sql = "select count(*)as counter from repairCategory where status is false";
		$result = $this->db->query($sql);
		if($result && $result->num_rows() > 0){
			return $result->row()->counter;
		}else{
			return 0;
		}
	}


	/*

		Repair Fault Section

	*/

	function getRepairFaults($show_inactive = 0){
		if($show_inactive == true){
			$sql = "select rf.*,rc.categoryName from repairFaults as rf
					left join repairCategory as rc on rc.categoryID = rf.categoryID";
		}else{
			$sql = "select rf.*,rc.categoryName from repairFaults as rf
					left join repairCategory as rc on rc.categoryID = rf.categoryID
					where rf.status is true";
		}

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->result();
		}else{
			return array();
		}
	}

	function createrepairFault($data){
		$sql = "insert into repairFaults(
			faultName,categoryID,created,createdBy,lastUpdated
			)values(
			'{$data['faultName']}',
			{$data['categoryID']},
			'{$data['created']}',
			{$data['createdBy']},
			'{$data['lastUpdated']}')";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	function editrepairFault($id = null,$inputData = null){

		$sql = "update repairFaults set
				faultName = '{$inputData['faultName']}',
				categoryID = {$inputData['categoryID']},
				lastUpdated = '{$inputData['lastUpdated']}'
				where faultID = $id";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	function getrepairFault($id = null){
		$sql = "select rf.*,
				u.username as createdByuser,
				rc.categoryName
				from repairFaults rf
				left join users as u on u.id = rf.createdby
				left join repairCategory as rc on rc.categoryID = rf.categoryID
				where rf.faultID = $id";

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->row();
		}else{
			return array();
		}
	}

	function changerepairFaultStatus($inputData){
		$sql = "update repairFaults
				set status = {$inputData['status']},
				lastUpdated = now() 
				where faultID = {$inputData['id']}";
		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}	
	}

	function getFaultSelectList(){
		$sql = "select faultID, 
				faultName,
				rc.categoryID
				from repairFaults
				left join repairCategory as rc on rc.categoryID = repairFaults.categoryID
				where repairFaults.status is true";

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){

			return $result->result();
		}

	}

	function getrepairFaultInactiveCount(){
		$sql = "select count(*)as counter from repairFaults where status is false";
		$result = $this->db->query($sql);
		if($result && $result->num_rows() > 0){
			return $result->row()->counter;
		}else{
			return 0;
		}		
	}


	/*

		Repair Work Done Section

	*/

	function repairWorkDoneListing($show_inactive = 0){
		if($show_inactive == true){
			$sql = "select rwd.*,
					rc.categoryID,
					rc.categoryName,
					rf.faultName
					from repairWorkDone as rwd
					left join repairFaults as rf on rf.faultID = rwd.faultID
					left join repairCategory as rc on rc.categoryID = rf.categoryID";
		}else{
			$sql = "select rwd.*,
					rc.categoryID,
					rc.categoryName,
					rf.faultName
					from repairWorkDone as rwd
					left join repairFaults as rf on rf.faultID = rwd.faultID
					left join repairCategory as rc on rc.categoryID = rf.categoryID
					where rwd.status is true";
		}

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->result();
		}else{
			return array();
		}
	}

	function createrepairWorkDone($data){
		$sql = "insert into repairWorkDone(
			workDoneName, workDoneCost,faultID,created,createdBy,lastUpdated
			)values(
			'{$data['workDoneName']}',
			{$data['workDoneCost']},
			'{$data['faultID']}',
			'{$data['created']}',
			{$data['createdBy']},
			'{$data['lastUpdated']}')";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	function editrepairWorkDone($id = null,$inputData = null){

		$sql = "update repairWorkDone set
				workDoneName = '{$inputData['workDoneName']}',
				workDoneCost = {$inputData['workDoneCost']},
				faultID = '{$inputData['faultID']}',
				lastUpdated = '{$inputData['lastUpdated']}'
				where workDoneID = $id";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	function getrepairWorkDone($id = null){
		$sql = "select rwd.*,
					rc.categoryID,
					rc.categoryName,
					rf.faultName,
					u.username as createdByuser
					from repairWorkDone as rwd
					left join repairFaults as rf on rf.faultID = rwd.faultID
					left join repairCategory as rc on rc.categoryID = rf.categoryID
					left join users as u on u.id = rwd.createdBy
					where rwd.workDoneID = $id";

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->row();
		}else{
			return array();
		}
	}

	function changerepairWorkDoneStatus($inputData){
		$sql = "update repairWorkDone
				set status = {$inputData['status']},
				lastUpdated = now() 
				where workDoneID = {$inputData['id']}";
		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}	
	}

	function getrepairWorkDoneInactiveCount(){
		$sql = "select count(*)as counter from repairWorkDone where status is false";
		$result = $this->db->query($sql);
		if($result && $result->num_rows() > 0){
			return $result->row()->counter;
		}else{
			return 0;
		}			
	}

	function getWorkDoneSelectList(){
		$sql = "select workDoneID,workDoneName,faultID from repairWorkDone where status is true";

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->result();
		}else{
			return array();
		}


	}

	/*

		Repair Work Done Section

	*/

	function repairCauseListing($show_inactive = 0){
		if($show_inactive == true){
			$sql = "select rc.*,
					cat.categoryName,
					rf.faultName,
					rwd.workDoneName
					from repairCause as rc
					left join repairWorkDone as rwd on rwd.workDoneID = rc.workDoneID
					left join repairFaults as rf on rf.faultID = rwd.faultID
					left join repairCategory as cat on cat.categoryID = rf.categoryID";
		}else{
			$sql = "select rc.*,
					cat.categoryName,
					rf.faultName,
					rwd.workDoneName
					from repairCause as rc
					left join repairWorkDone as rwd on rwd.workDoneID = rc.workDoneID
					left join repairFaults as rf on rf.faultID = rwd.faultID
					left join repairCategory as cat on cat.categoryID = rf.categoryID
					where rc.status is true";
		}

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->result();
		}else{
			return array();
		}
	}

	function createrepairCause($data){
		$sql = "insert into repairWorkDone(
			workDoneName, workDoneCost,faultID,created,createdBy,lastUpdated
			)values(
			'{$data['workDoneName']}',
			{$data['workDoneCost']},
			'{$data['faultID']}',
			'{$data['created']}',
			{$data['createdBy']},
			'{$data['lastUpdated']}')";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	function editrepairCause($id = null,$inputData = null){

		$sql = "update repairWorkDone set
				workDoneName = '{$inputData['workDoneName']}',
				workDoneCost = {$inputData['workDoneCost']},
				faultID = '{$inputData['faultID']}',
				lastUpdated = '{$inputData['lastUpdated']}'
				where workDoneID = $id";

		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	function getrepairCause($id = null){
		$sql = "select rwd.*,
					rc.categoryID,
					rc.categoryName,
					rf.faultName,
					u.username as createdByuser
					from repairWorkDone as rwd
					left join repairFaults as rf on rf.faultID = rwd.faultID
					left join repairCategory as rc on rc.categoryID = rf.categoryID
					left join users as u on u.id = rwd.createdBy
					where rwd.workDoneID = $id";

		$result = $this->db->query($sql);

		if($result && $result->num_rows() > 0){
			return $result->row();
		}else{
			return array();
		}
	}

	function changerepairCauseStatus($inputData){
		$sql = "update repairWorkDone
				set status = {$inputData['status']},
				lastUpdated = now() 
				where workDoneID = {$inputData['id']}";
		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}	
	}

	function getrepairCauseInactiveCount(){
		$sql = "select count(*)as counter from repairWorkDone where status is false";
		$result = $this->db->query($sql);
		if($result && $result->num_rows() > 0){
			return $result->row()->counter;
		}else{
			return 0;
		}			
	}


}
?>