<?php

if(!defined("BASEPATH"))exit('Direct script access not allowed');


class StockAdmin extends CI_Controller{


	function __construct(){
		parent::__construct();

		if(!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}

		$this->load->model('StockAdminModel');
	}

	function productTypeListing($show_inactive = 0){
		$this->data['content'] = 'stock/productTypeListing';
		$this->data['title'] = lang('stockProductTypeListingHeading');
		$this->data['show_inactive'] = $show_inactive;
		$this->data['results'] = $this->StockAdminModel->getProductTypes($show_inactive);

		$this->load->view('includes/template',$this->data);
	}


	function createProductType(){


		$this->form_validation->set_Rules('productTypeName','Product Type Name','required');
		$this->form_validation->set_Rules('productType','Product Type','required|numeric');
		$this->form_validation->set_Rules('productTypeAbbr','Product Type Abbr','required');

		if($this->form_validation->run() == false){
			//show create product type form

			$this->data['title'] = lang('stockCreateProductTypeHeading');
			$this->data['content'] = 'stock/createProductType';
			$this->load->view('includes/template',$this->data);
		}else{
			//save record

			$inputData['productTypeName'] = $_POST['productTypeName'];
			$inputData['productType'] = $_POST['productType'];
			$inputData['productTypeAbbr'] = $_POST['productTypeAbbr'];
			$inputData['created'] = date('Y-m-d H:i:s');
			$inputData['createdBy'] = $this->session->userdata('user_id');
			$inputData['lastUpdated'] = date('Y-m-d H:i:s');

			if($this->StockAdminModel->CreateProductType($inputData)){
				redirect('StockAdmin/productTypeListing');
			}else{
				show_404();
			}

		}
	}


	function changeProductTypeStatus($id = null, $status = 0){
		if(is_Null($id) || is_Null($status)){redirect('StockAdmin/productTypeListing');};

		$inputData['id'] = $id;
		$inputData['status'] = $status;
		if($this->StockAdminModel->changeProductTypeStatus($inputData)){
			redirect('StockAdmin/productTypeListing');
		}else{
			show_404();
		}		
	}

	function viewProductType($id = null){
		$this->data['title'] = lang('stockViewProductTypeHeading');
		$this->data['content'] = 'stock/viewProductType';
		$this->data['result'] = $this->StockAdminModel->getProductType($id);

		$this->load->view('includes/template',$this->data);
	}

	function editProductType($id = null){

		$this->form_validation->set_Rules('productTypeName','Product Type Name','required');
		$this->form_validation->set_Rules('productType','Product Type','required|numeric');
		$this->form_validation->set_Rules('productTypeAbbr','Product Type Abbr','required');

		if($this->form_validation->run() == false){
			//show create product type form

			$this->data['title'] = lang('stockEditProductTypeHeading');
			$this->data['content'] = 'stock/editProductType';
			$this->data['result'] = $this->StockAdminModel->getProductType($id);			
			$this->load->view('includes/template',$this->data);
		}else{
			//save record

			$inputData['productTypeName'] = $_POST['productTypeName'];
			$inputData['productType'] = $_POST['productType'];
			$inputData['productTypeAbbr'] = $_POST['productTypeAbbr'];
			$inputData['lastUpdated'] = date('Y-m-d H:i:s');

			if($this->StockAdminModel->EditProductType($_POST['id'],$inputData)){
				redirect('StockAdmin/productTypeListing');
			}else{
				show_404();
			}

		}		
	}
}

?>