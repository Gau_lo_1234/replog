<?php

if(!defined('BASEPATH'))exit('Direct script access not allowed');


class ClientAdmin extends CI_Controller{

	function __construct(){
		parent::__construct();

		if(!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}

		if(!$this->ion_auth->is_superadmin() && !$this->ion_auth->is_admin()){
			echo "You must be an Administrator to view this page";
		}

		$this->load->model('ClientAdminModel');
		$this->load->model('CompanyProfileModel','Companies');
	}


	function index($show_inactive = 0){



		$this->data['content'] = "clients/clientListing";
		$this->data['title'] = lang('clientListingHeading');
		$this->data['show_inactive'] = ($show_inactive == true)?true:false;
		$this->data['results'] = ($this->ion_auth->is_superadmin())
			? $this->ClientAdminModel->getClients($show_inactive)
			: $this->ClientAdminModel->getClients($show_inactive,$this->session->userdata('companyID'));
		 

		$this->load->view('includes/template',$this->data);		
	}

	public function createClient(){

		$this->form_validation->set_Rules('clientName','Client Name','required|trim');
		$this->form_validation->set_Rules('clientEmail','Client Email','trim|valid_email');
		$this->form_validation->set_Rules('clientCompanyNumber','Client Company Number','max_length[20]|trim');
		$this->form_validation->set_Rules('clientVatNumber','Client Vat Number','max_length[13]|trim');
		$this->form_validation->set_Rules('accountTypeID','Client Account Type','required');		
		$this->form_validation->set_Rules('contactPerson','Contact Person','trim');



                if ($this->form_validation->run() == FALSE)
                {


					$this->data['content'] = "clients/createClient";
					$this->data['title'] = lang('createClientHeading');
					$this->data['companies'] = $this->Companies->getProfiles(1);
					$this->data['accountTypes'] = $this->ClientAdminModel->getAccountTypes(1);

					$this->load->view('includes/template',$this->data);
				
                }
                else
                {

                		$postData['companyID'] = $_POST['companyID'];
                		$postData['accountTypeID'] = $_POST['accountTypeID'];
                        $postData['clientName'] = $_POST['clientName'];
                        $postData['clientCompanyNumber'] = $_POST['clientCompanyNumber'];
                        $postData['clientVatNumber'] = $_POST['clientVatNumber'];
                        $postData['contactPerson'] = $_POST['contactPerson'];
                        $postData['clientTelephone'] = $_POST['clientTelephone'];
                        $postData['clientEmail'] = $_POST['clientEmail'];
                        $postData['clientPhysicalAddress'] = $_POST['clientPhysicalAddress'];
                        $postData['clientPostalAddress'] = $_POST['clientPostalAddress'];
                        $postData['created'] = date('Y-m-d H:i:s');
                        $postData['createdBy'] = $this->session->userdata('user_id');

                        if($this->ClientAdminModel->createClient($postData)){

                        	redirect('ClientAdmin/index');
                        }else{
                        	show404();
                        }
                }
	}

	public function editClient($id = null){
		if(is_Null($id))redirect('ClientAdmin/index');

		$this->form_validation->set_Rules('clientName','Client Name','required|trim');
		$this->form_validation->set_Rules('clientEmail','Client Email','trim|valid_email');
		$this->form_validation->set_Rules('clientCompanyNumber','Client Company Number','max_length[20]|trim');
		$this->form_validation->set_Rules('clientVatNumber','Client Vat Number','max_length[13]|trim');
		$this->form_validation->set_Rules('accountTypeID','Client Account Type','required');		
		$this->form_validation->set_Rules('contactPerson','Contact Person','trim');



                if ($this->form_validation->run() == FALSE)
                {


					$this->data['content'] = "clients/editClient";
					$this->data['title'] = lang('editClientHeading');
					$this->data['companies'] = $this->Companies->getProfiles(1);
					$this->data['accountTypes'] = $this->ClientAdminModel->getAccountTypes(1);
					$this->data['client'] = $this->ClientAdminModel->getClient($id);

					$this->load->view('includes/template',$this->data);
				
                }
                else
                {

                		$postData['clientID'] = $_POST['clientID'];
                		$postData['companyID'] = $_POST['companyID'];
                		$postData['accountTypeID'] = $_POST['accountTypeID'];
                        $postData['clientName'] = $_POST['clientName'];
                        $postData['clientCompanyNumber'] = $_POST['clientCompanyNumber'];
                        $postData['clientVatNumber'] = $_POST['clientVatNumber'];
                        $postData['contactPerson'] = $_POST['contactPerson'];
                        $postData['clientTelephone'] = $_POST['clientTelephone'];
                        $postData['clientEmail'] = $_POST['clientEmail'];
                        $postData['clientPhysicalAddress'] = $_POST['clientPhysicalAddress'];
                        $postData['clientPostalAddress'] = $_POST['clientPostalAddress'];
                        $postData['lastUpdated'] = date('Y-m-d H:i:s');
                        $postData['lastUpdatedBy'] = $this->session->userdata('user_id');

                        if($this->ClientAdminModel->editClient($postData)){

                        	redirect('ClientAdmin/index');
                        }else{
                        	show404();
                        }
                }

	}

	public function viewClient($id = null){
		if(is_Null($id)){redirect('ClientAdmin/index');};


		$this->data['content'] = 'clients/viewClient';
		$this->data['title'] = lang('viewClientHeading');
		$this->data['client'] = $this->ClientAdminModel->getClient($id);

		$this->load->view('includes/template',$this->data);
	}

	public function changeClientStatus($id = null,$status = null){
		if(is_Null($id) || is_Null($status)){redirect('ClientAdmin/index');};

		$inputData['id'] = $id;
		$inputData['status'] = $status;
		if($this->ClientAdminModel->changeClientStatus($inputData)){
			redirect('ClientAdmin/Index');
		}else{
			show_404();
		}
	}
	function accountTypeListing($show_inactive = 0){
		$this->data['content'] = "clients/accountTypeListing";
		$this->data['title'] = lang('clientsAccountTypeListing');
		$this->data['show_inactive'] = ($show_inactive == true)?true:false;
		$this->data['results'] = $this->ClientAdminModel->getAccountTypes($show_inactive);

		$this->load->view('includes/template',$this->data);
	}

	function createClientAccountType(){

		$this->form_validation->set_Rules('accountTypeName','Account Type Name','required');
		$this->form_validation->set_Rules('description','description','required');
		$this->form_validation->set_Rules('companyID','Company Name','required');

                if ($this->form_validation->run() == FALSE)
                {


					$this->data['content'] = "clients/createClientAccountType";
					$this->data['title'] = lang('createClientAccountTypeHeading');
					$this->data['companies'] = $this->data['companies'] = $this->Companies->getProfiles(1);

					$this->load->view('includes/template',$this->data);
				
                }
                else
                {
                        $postData['accountTypeName'] = $_POST['accountTypeName'];
                        $postData['companyID'] = $_POST['companyID'];
                        $postData['description'] = $_POST['description'];
                        $postData['created'] = date('Y-m-d H:i:s');
                        $postData['createdBy'] = $this->session->userdata('user_id');
                        $postData['lastUpdated'] = date('Y-m-d H:i:s');

                        if($this->ClientAdminModel->createClientAccountType($postData)){
                        	redirect('ClientAdmin/accountTypeListing');
                        }else{
                        	show404();
                        }
                }

	}

	function editClientAccountType($id = null){

				if(is_Null($id)){redirect('ClientAdmin/accountTypeListing');};

				$this->form_validation->set_Rules('accountTypeName','Account Type Name','required');
				$this->form_validation->set_Rules('description','description','required');
				$this->form_validation->set_Rules('companyID','Company Name','required');


                if ($this->form_validation->run() == FALSE)
                {


					$this->data['content'] = "clients/editClientAccountType";
					$this->data['title'] = lang('editClientAccountTypeHeading');
					$this->data['result'] = $this->ClientAdminModel->getClientAccountType($id);
					$this->data['companies'] = $this->data['companies'] = $this->Companies->getProfiles(1);					
					$this->load->view('includes/template',$this->data);
				
                }
                else
                {
                        $postData['accountTypeName'] = $_POST['accountTypeName'];
                        $postData['companyID'] = $_POST['companyID'];
                        $postData['description'] = $_POST['description'];
                        $postData['lastUpdated'] = date('Y-m-d H:i:s');

                        if($this->ClientAdminModel->updateClientAccountType($_POST['id'],$postData)){
                        	redirect('ClientAdmin/accountTypeListing');
                        }else{
                        	show404();
                        }
                }
	}

	function viewClientAccountType($id = null){

		if(is_Null($id)){redirect('ClientAdmin/accountTypeListing');};


		$this->data['content'] = 'clients/viewClientAccountType';
		$this->data['title'] = lang('viewClientAccountType');
		$this->data['result'] = $this->ClientAdminModel->getClientAccountType($id);

		$this->load->view('includes/template',$this->data);
 	}

	function changeAccountTypeStatus($id = null, $status = null){
		if(is_Null($id) || is_Null($status)){redirect('ClientAdmin/accountTypeListing');};

		$inputData['id'] = $id;
		$inputData['status'] = $status;
		if($this->ClientAdminModel->changeAccountTypeStatus($inputData)){
			redirect('ClientAdmin/accountTypeListing');
		}else{
			show_404();
		}
	}
}

?>