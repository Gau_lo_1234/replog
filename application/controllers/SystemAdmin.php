<?php if(!defined('BASEPATH'))die('Direct script access not allowed');

class SystemAdmin extends CI_Controller{


		function __construct(){
			parent::__construct();
			if(!$this->ion_auth->logged_in()){
				redirect('auth/login');
			}

			$this->load->model('SystemAdminModel');
		}


		/*

			INITIAL OBSERVATIONS SECTION

		*/

		function initObservationsListing($show_inactive = 0){
			$this->data['title'] = lang('systemInitialObservationsListingHeading');
			$this->data['content'] = 'system/initialObservationListing';
			$this->data['results'] = $this->SystemAdminModel->getInitialObservations($show_inactive);
			$this->data['show_inactive'] = $show_inactive;

			$this->load->view('includes/template',$this->data);
		}

		function createInitialObservation(){
			$this->form_validation->set_rules('observation','Initial Observation','required');


			if($this->form_validation->run() == false){
				//load create new observation form
				$this->data['title'] = lang('systemCreateInitialObservationHeading');
				$this->data['content'] = 'system/createInitialObservation';

				$this->load->view('includes/template',$this->data);
			}else{
				//save information and return to listing
				$uploadData['observation'] = $_POST['observation'];
				$uploadData['created'] = date('Y-m-d H:i:S');
				$uploadData['createdBy'] = $this->session->userdata('user_id');
				$uploadData['lastUpdated'] = date('Y-m-d H:i:s');

				if($this->SystemAdminModel->createInitialObservation($uploadData)){
					redirect('SystemAdmin/initObservationsListing');
				}else{
					show_404();
				}
			}
		}

		function editInitialObservation($id = null){
			$this->form_validation->set_rules('observation','Initial Observation','required');


			if($this->form_validation->run() == false){
				//load create new observation form
				$this->data['title'] = lang('systemEditInitialObservationHeading');
				$this->data['content'] = 'system/editInitialObservation';
				$this->data['result'] = $this->SystemAdminModel->getInitialObservation($id);

				$this->load->view('includes/template',$this->data);
			}else{
				//save information and return to listing
				$uploadData['observation'] = $_POST['observation'];
				$uploadData['lastUpdated'] = date('Y-m-d H:i:s');

				if($this->SystemAdminModel->editInitialObservation($_POST['id'],$uploadData)){
					redirect('SystemAdmin/initObservationsListing');
				}else{
					show_404();
				}
			}
		}

		function viewInitialObservation($id = null){
			$this->data['title'] = lang('systemViewInitialObservationHeading');
			$this->data['content'] = 'system/viewInitialObservation';
			$this->data['result'] = $this->SystemAdminModel->getInitialObservation($id);

			$this->load->view('includes/template',$this->data);
		}

		function changeInitialObservationStatus($id = null,$status = 0){
			if(is_Null($id) || is_Null($status)){redirect('SystemAdmin/initObservationsListing');};

			$inputData['id'] = $id;
			$inputData['status'] = $status;
			if($this->SystemAdminModel->changeinitObservationStatus($inputData)){
				redirect('SystemAdmin/initObservationsListing');
			}else{
				show_404();
			}	
		}		


		/*

			REPAIR CATEGORIES SECTION

		*/

		function repairCategoryListing($show_inactive = 0){
			$this->data['title'] = lang('systemrepairCategoryListingHeading');
			$this->data['content'] = 'system/repairCategoryListing';
			$this->data['results'] = $this->SystemAdminModel->getRepairCategorys($show_inactive);
			$this->data['show_inactive'] = $show_inactive;
			$this->data['inactiveCount'] = $this->SystemAdminModel->getrepairCategoryInactiveCount();

			$this->load->view('includes/template',$this->data);
		}

		function createrepairCategory(){
			$this->form_validation->set_rules('categoryName','Category','required');


			if($this->form_validation->run() == false){
				//load create new observation form
				$this->data['title'] = lang('systemCreaterepairCategoryHeading');
				$this->data['content'] = 'system/createrepairCategory';

				$this->load->view('includes/template',$this->data);
			}else{
				//save information and return to listing
				$uploadData['categoryName'] = $_POST['categoryName'];
				$uploadData['created'] = date('Y-m-d H:i:S');
				$uploadData['createdBy'] = $this->session->userdata('user_id');
				$uploadData['lastUpdated'] = date('Y-m-d H:i:s');

				if($this->SystemAdminModel->createrepairCategory($uploadData)){
					redirect('SystemAdmin/repairCategoryListing');
				}else{
					show_404();
				}
			}
		}

		function editrepairCategory($id = null){
			$this->form_validation->set_rules('categoryName','Category','required');


			if($this->form_validation->run() == false){
				//load create new observation form
				$this->data['title'] = lang('systemEditrepairCategoryHeading');
				$this->data['content'] = 'system/editrepairCategory';
				$this->data['result'] = $this->SystemAdminModel->getrepairCategory($id);

				$this->load->view('includes/template',$this->data);
			}else{
				//save information and return to listing
				$uploadData['categoryName'] = $_POST['categoryName'];
				$uploadData['lastUpdated'] = date('Y-m-d H:i:s');

				if($this->SystemAdminModel->editrepairCategory($_POST['id'],$uploadData)){
					redirect('SystemAdmin/repairCategoryListing');
				}else{
					show_404();
				}
			}
		}

		function viewrepairCategory($id = null){
			$this->data['title'] = lang('systemViewrepairCategoryHeading');
			$this->data['content'] = 'system/viewrepairCategory';
			$this->data['result'] = $this->SystemAdminModel->getrepairCategory($id);

			$this->load->view('includes/template',$this->data);
		}

		function changerepairCategoryStatus($id = null,$status = 0){
			if(is_Null($id) || is_Null($status)){redirect('SystemAdmin/repairCategoryListing');};

			$inputData['id'] = $id;
			$inputData['status'] = $status;
			if($this->SystemAdminModel->changerepairCategoryStatus($inputData)){
				redirect('SystemAdmin/repairCategoryListing');
			}else{
				show_404();
			}	
		}

		/*

			REPAIR FAULTS SECTION

		*/

		function repairFaultListing($show_inactive = 0){
			$this->data['title'] = lang('systemrepairFaultListingHeading');
			$this->data['content'] = 'system/repairFaultListing';
			$this->data['results'] = $this->SystemAdminModel->getRepairFaults($show_inactive);
			$this->data['inactiveCount'] = $this->SystemAdminModel->getrepairFaultInactiveCount();
			$this->data['show_inactive'] = $show_inactive;

			$this->load->view('includes/template',$this->data);
		}

		function createrepairFault(){
			$this->form_validation->set_rules('faultName','Category','required');
			$this->form_validation->set_rules('categoryID','Category Name','required|numeric');

			if($this->form_validation->run() == false){
				//load create new observation form
				$this->data['title'] = lang('systemCreaterepairFaultHeading');
				$this->data['content'] = 'system/createrepairFault';
				$this->data['categorySelect'] = $this->SystemAdminModel->getCategorySelectList();

				$this->load->view('includes/template',$this->data);
			}else{
				//save information and return to listing
				$uploadData['faultName'] = $_POST['faultName'];
				$uploadData['categoryID'] = $_POST['categoryID'];
				$uploadData['created'] = date('Y-m-d H:i:S');
				$uploadData['createdBy'] = $this->session->userdata('user_id');
				$uploadData['lastUpdated'] = date('Y-m-d H:i:s');

				if($this->SystemAdminModel->createrepairFault($uploadData)){
					redirect('SystemAdmin/repairFaultListing');
				}else{
					show_404();
				}
			}
		}

		function editrepairFault($id = null){
			$this->form_validation->set_rules('faultName','Fault','required');
			$this->form_validation->set_rules('categoryID','Category Name','required');

			if($this->form_validation->run() == false){
				//load create new observation form
				$this->data['title'] = lang('systemEditrepairFaultHeading');
				$this->data['content'] = 'system/editrepairFault';
				$this->data['result'] = $this->SystemAdminModel->getrepairFault($id);
				$this->data['categorySelect'] = $this->SystemAdminModel->getCategorySelectList();
		
				$this->load->view('includes/template',$this->data);
			}else{
				//save information and return to listing
				$uploadData['faultName'] = $_POST['faultName'];
				$uploadData['categoryID'] = $_POST['categoryID'];
				$uploadData['lastUpdated'] = date('Y-m-d H:i:s');

				if($this->SystemAdminModel->editrepairFault($_POST['id'],$uploadData)){
					redirect('SystemAdmin/repairFaultListing');
				}else{
					show_404();
				}
			}
		}

		function viewrepairFault($id = null){
			$this->data['title'] = lang('systemViewrepairFaultHeading');
			$this->data['content'] = 'system/viewrepairFault';
			$this->data['result'] = $this->SystemAdminModel->getrepairFault($id);

			$this->load->view('includes/template',$this->data);
		}

		function changerepairFaultStatus($id = null,$status = 0){
			if(is_Null($id) || is_Null($status)){redirect('SystemAdmin/repairFaulktListing');};

			$inputData['id'] = $id;
			$inputData['status'] = $status;
			if($this->SystemAdminModel->changerepairFaultStatus($inputData)){
				redirect('SystemAdmin/repairFaultListing');
			}else{
				show_404();
			}	
		}

		/*

			REPAIR WORKDONE SECTION

		*/

		function repairWorkDoneListing($show_inactive = 0){
			$this->data['title'] = lang('systemrepairWorkDoneListingHeading');
			$this->data['content'] = 'system/repairWorkDoneListing';
			$this->data['results'] = $this->SystemAdminModel->repairWorkDoneListing($show_inactive);
			$this->data['inactiveCount'] = $this->SystemAdminModel->getrepairWorkDoneInactiveCount();
			$this->data['show_inactive'] = $show_inactive;

			$this->load->view('includes/template',$this->data);
		}

		function createrepairWorkDone(){
			$this->form_validation->set_rules('workDoneName','Work Done','required');
			$this->form_validation->set_rules('workDoneCost','Work Done Cost','required|numeric');
			$this->form_validation->set_rules('faultID','Fault Name','required|numeric');
			$this->form_validation->set_rules('categoryID','Category Name','required|numeric');

			if($this->form_validation->run() == false){
				//load create new observation form
				$this->data['title'] = lang('systemCreaterepairWorkDoneHeading');
				$this->data['content'] = 'system/createrepairWorkDone';
				$this->data['categories'] = $this->SystemAdminModel->getCategorySelectList();
				$this->data['faults'] = $this->SystemAdminModel->getFaultSelectList();
				

				$this->load->view('includes/template',$this->data);
			}else{
				//save information and return to listing
				$uploadData['workDoneName'] = $_POST['workDoneName'];
				$uploadData['workDoneCost'] = $_POST['workDoneCost'];
				$uploadData['faultID'] = $_POST['faultID'];
				$uploadData['created'] = date('Y-m-d H:i:S');
				$uploadData['createdBy'] = $this->session->userdata('user_id');
				$uploadData['lastUpdated'] = date('Y-m-d H:i:s');

				if($this->SystemAdminModel->createrepairWorkDone($uploadData)){
					redirect('SystemAdmin/repairWorkDoneListing');
				}else{
					show_404();
				}
			}
		}

		function editrepairWorkDone($id = null){
			$this->form_validation->set_rules('workDoneName','Work Done','required');
			$this->form_validation->set_rules('workDoneCost','Work Done Cost','required|numeric');
			$this->form_validation->set_rules('faultID','Fault Name','required|numeric');
			$this->form_validation->set_rules('categoryID','Category Name','required|numeric');

			if($this->form_validation->run() == false){
				//load create new observation form
				$this->data['title'] = lang('systemEditrepairWorkDoneHeading');
				$this->data['content'] = 'system/editrepairWorkDone';
				$this->data['categories'] = $this->SystemAdminModel->getCategorySelectList();
				$this->data['faults'] = $this->SystemAdminModel->getFaultSelectList();
				$this->data['result'] = $this->SystemAdminModel->getrepairWorkDone($id);

				$this->load->view('includes/template',$this->data);
			}else{
				//save information and return to listing
				$uploadData['workDoneName'] = $_POST['workDoneName'];
				$uploadData['workDoneCost'] = $_POST['workDoneCost'];
				$uploadData['faultID'] = $_POST['faultID'];
				$uploadData['lastUpdated'] = date('Y-m-d H:i:s');

				if($this->SystemAdminModel->editrepairWorkDOne($_POST['id'],$uploadData)){
					redirect('SystemAdmin/repairWorkDoneListing');
				}else{
					show_404();
				}
			}
		}

		function viewrepairWorkDone($id = null){
			$this->data['title'] = lang('systemViewrepairWorkDoneHeading');
			$this->data['content'] = 'system/viewrepairWorkDone';
			$this->data['result'] = $this->SystemAdminModel->getrepairWorkDone($id);

			$this->load->view('includes/template',$this->data);
		}

		function changerepairWorkDoneStatus($id = null,$status = 0){
			if(is_Null($id) || is_Null($status)){redirect('SystemAdmin/repairWorkDoneListing');};

			$inputData['id'] = $id;
			$inputData['status'] = $status;
			if($this->SystemAdminModel->changerepairWorkDoneStatus($inputData)){
				redirect('SystemAdmin/repairWorkDoneListing');
			}else{
				show_404();
			}	
		}	

		/*

			REPAIR CAUSE SECTION

		*/

		function repairCauseListing($show_inactive = 0){
			$this->data['title'] = lang('systemrepairCauseListingHeading');
			$this->data['content'] = 'system/repairCauseListing';
			$this->data['results'] = $this->SystemAdminModel->repairCauseListing($show_inactive);
			$this->data['inactiveCount'] = $this->SystemAdminModel->getrepairCauseInactiveCount();
			$this->data['show_inactive'] = $show_inactive;

			$this->load->view('includes/template',$this->data);
		}

		function createrepairCause(){
			$this->form_validation->set_rules('causeName','Name','required');
			$this->form_validation->set_rukles('causeDesc','Description','required');
			$this->form_validation->set_rules('workDoneID','Work Done Name','required');
			$this->form_validation->set_rules('faultID','Fault Name','required|numeric');
			$this->form_validation->set_rules('categoryID','Category Name','required|numeric');

			if($this->form_validation->run() == false){
				//load create new observation form
				$this->data['title'] = lang('systemCreaterepairCauseHeading');
				$this->data['content'] = 'system/createrepairCause';
				$this->data['categories'] = $this->SystemAdminModel->getCategorySelectList();
				$this->data['faults'] = $this->SystemAdminModel->getFaultSelectList();
				$this->data['workDone'] = $this->SystemAdminModel->getWorkDoneSelectList();
				

				$this->load->view('includes/template',$this->data);
			}else{
				//save information and return to listing
				$uploadData['workDoneName'] = $_POST['workDoneName'];
				$uploadData['workDoneCost'] = $_POST['workDoneCost'];
				$uploadData['faultID'] = $_POST['faultID'];
				$uploadData['created'] = date('Y-m-d H:i:S');
				$uploadData['createdBy'] = $this->session->userdata('user_id');
				$uploadData['lastUpdated'] = date('Y-m-d H:i:s');

				if($this->SystemAdminModel->createrepairWorkDone($uploadData)){
					redirect('SystemAdmin/repairWorkDoneListing');
				}else{
					show_404();
				}
			}
		}

		function editrepairCause($id = null){
			$this->form_validation->set_rules('workDoneName','Work Done','required');
			$this->form_validation->set_rules('workDoneCost','Work Done Cost','required|numeric');
			$this->form_validation->set_rules('faultID','Fault Name','required|numeric');
			$this->form_validation->set_rules('categoryID','Category Name','required|numeric');

			if($this->form_validation->run() == false){
				//load create new observation form
				$this->data['title'] = lang('systemEditrepairWorkDoneHeading');
				$this->data['content'] = 'system/editrepairWorkDone';
				$this->data['categories'] = $this->SystemAdminModel->getCategorySelectList();
				$this->data['faults'] = $this->SystemAdminModel->getFaultSelectList();
				$this->data['result'] = $this->SystemAdminModel->getrepairWorkDone($id);

				$this->load->view('includes/template',$this->data);
			}else{
				//save information and return to listing
				$uploadData['workDoneName'] = $_POST['workDoneName'];
				$uploadData['workDoneCost'] = $_POST['workDoneCost'];
				$uploadData['faultID'] = $_POST['faultID'];
				$uploadData['lastUpdated'] = date('Y-m-d H:i:s');

				if($this->SystemAdminModel->editrepairWorkDOne($_POST['id'],$uploadData)){
					redirect('SystemAdmin/repairWorkDoneListing');
				}else{
					show_404();
				}
			}
		}

		function viewrepairCause($id = null){
			$this->data['title'] = lang('systemViewrepairWorkDoneHeading');
			$this->data['content'] = 'system/viewrepairWorkDone';
			$this->data['result'] = $this->SystemAdminModel->getrepairWorkDone($id);

			$this->load->view('includes/template',$this->data);
		}

		function changerepairCausetatus($id = null,$status = 0){
			if(is_Null($id) || is_Null($status)){redirect('SystemAdmin/repairWorkDoneListing');};

			$inputData['id'] = $id;
			$inputData['status'] = $status;
			if($this->SystemAdminModel->changerepairWorkDoneStatus($inputData)){
				redirect('SystemAdmin/repairWorkDoneListing');
			}else{
				show_404();
			}	
		}	


}





	?>