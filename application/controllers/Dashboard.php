<?php 


if(!DEFINED('BASEPATH'))die('Direct script access not allowed');


Class Dashboard extends CI_Controller{


	function __construct(){
		parent::__construct();

		if(!$this->ion_auth->logged_in()){

			redirect('auth/login');
		}
	}

	function index(){
		$this->data['title'] = lang('replog_app_title').' - '.$this->lang->line('replog_dashboard');
		$this->data['content'] = 'generic/dashboard';
		$this->load->view('includes/template',$this->data);
	}

}
?>