<?php

	if(!defined('BASEPATH'))die("Direct script access not allowed");

	class CompanyProfileAdmin extends CI_Controller{


		function __construct(){
			parent::__construct();
			$this->load->model('CompanyProfileModel');
		}


	/* Load company Profile Admin Page*/
	public function compProfiles($show_inactive = 0)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_superadmin() && !$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be a Administrator to view this page.');
		}
		else
		{

			$this->data['title'] = lang('companyProfileListingHeading');
			$this->data['content'] = 'system/compProfiles';
			$this->data['show_inactive'] = $show_inactive;
			$this->data['results'] = ($this->ion_auth->is_superadmin())?$this->CompanyProfileModel->getProfiles($show_inactive):$this->CompanyProfileModel->getProfiles($show_inactive,$this->session->userdata('companyID'));

			$this->load->view('includes/template',$this->data);
		}
	}


		function createCompanyProfile(){
			$this->form_validation->set_rules('companyName','Company Name','required');
			$this->form_validation->set_rules('companyContactName','Contact Name','required');
			$this->form_validation->set_rules('companyContactTel','Contact Tel','required');
			$this->form_validation->set_rules('companyContactEmail','Contact Email','required');
			$this->form_validation->set_rules('branchName','Default Branch Name','required');


			if($this->form_validation->run() == false){
				//load create new observation form
				$this->data['title'] = lang('systemCreateCompanyProfile');
				$this->data['content'] = 'system/createCompanyProfile';

				$this->load->view('includes/template',$this->data);
			}else{
				//save information and return to listing
				$uploadData['companyName'] = strtoupper($_POST['companyName']);

				$uploadData['companyContactName'] = $_POST['companyContactName'];
				$uploadData['companyContactTel'] = $_POST['companyContactTel'];
				$uploadData['companyContactEmail'] = $_POST['companyContactEmail'];				
				$uploadData['created'] = date('Y-m-d H:i:S');
				$uploadData['createdBy'] = $this->session->userdata('user_id');
				$uploadData['lastUpdated'] = date('Y-m-d H:i:s');

				if($insertID = $this->CompanyProfileModel->createCompanyProfile($uploadData)){
				
					//create matching branchProfile Record
					$uploadData['companyID'] = $insertID;
					$uploadData['branchName'] = strtoupper($_POST['branchName']);
					$uploadData['branchContactName'] = $_POST['companyContactName'];
					$uploadData['branchContactTel'] = $_POST['companyContactTel'];
					$uploadData['branchContactEmail'] = $_POST['companyContactEmail'];						
					if($branchID = $this->CompanyProfileModel->createBranchProfile($uploadData)){
				
						redirect('CompanyProfileAdmin/compProfiles');
					}				
					
				}else{
					show_404();
				}
			}
		}


		function editCompanyProfile($id = null){
			$this->form_validation->set_rules('companyName','Company Name','required');
			$this->form_validation->set_rules('companyContactName','Contact Name','required');
			$this->form_validation->set_rules('companyContactTel','Contact Tel','required');
			$this->form_validation->set_rules('companyContactEmail','Contact Email','required|valid_email');


			if($this->form_validation->run() == false){
				//load create new observation form
				$this->data['title'] = lang('systemEditCompanyProfile');
				$this->data['content'] = 'system/editCompanyProfile';
				$this->data['profile'] = $this->CompanyProfileModel->getCompanyProfile($id);

				$this->load->view('includes/template',$this->data);
			}else{


				//save information and return to listing
				$uploadData['companyName'] = strtoupper($_POST['companyName']);

				$uploadData['companyContactName'] = $_POST['companyContactName'];
				$uploadData['companyContactTel'] = $_POST['companyContactTel'];
				$uploadData['companyContactEmail'] = $_POST['companyContactEmail'];				
				$uploadData['lastUpdated'] = date('Y-m-d H:i:s');
				$uploadData['lastUpdatedBy'] = $this->session->userdata('user_id');
				$uploadData['companyID'] = $_POST['companyID'];

				if($insertID = $this->CompanyProfileModel->editCompanyProfile($_POST['companyID'],$uploadData)){
				
						redirect('CompanyProfileAdmin/compProfiles');
							
					
				}else{
					show_404();
				}
			}
		}

		function changeProfileStatus($id =null,$status = null){
			if($this->CompanyProfileModel->changeProfileStatus($id,$status)){
				redirect('CompanyProfileAdmin/compProfiles');
			}else{
				show_404();
			}
		}

		function viewProfile($id = null){
			$this->data['title'] = lang('systemViewCompanyProfileHeading');
			$this->data['content'] = 'system/viewCompanyProfile';
			$this->data['result'] = $this->CompanyProfileModel->getCompanyProfile($id);
			$this->data['branches'] = $this->CompanyProfileModel->getCompanyBranches($id);

			$this->load->view('includes/template',$this->data);
		}

		function changeInitialObservationStatus($id = null,$status = 0){
			if(is_Null($id) || is_Null($status)){redirect('SystemAdmin/initObservationsListing');};

			$inputData['id'] = $id;
			$inputData['status'] = $status;
			if($this->SystemAdminModel->changeinitObservationStatus($inputData)){
				redirect('SystemAdmin/initObservationsListing');
			}else{
				show_404();
			}	
		}	


		function companyBranchListing($id = null,$show_inactive = 0){

			if (!$this->ion_auth->logged_in())
			{
				// redirect them to the login page
				redirect('auth/login', 'refresh');
			}
			else if (!$this->ion_auth->is_superadmin() && !$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
			{
				// redirect them to the home page because they must be an administrator to view this
				return show_error('You must be a Super Administrator to view this page.');
			}
			else
			{

				$this->data['title'] = lang('companyBranchListingHeading');
				$this->data['content'] = 'system/compBranchListing';
				$this->data['show_inactive'] = $show_inactive;
				$this->data['results'] = $this->CompanyProfileModel->getCompanyBranches($id,$show_inactive);
				$this->data['companyID'] = $id;

				$this->load->view('includes/template',$this->data);
			}			
		}

		function createBranchProfile($companyID = null){
			$this->form_validation->set_rules('branchName','Branch Name','required');
			$this->form_validation->set_rules('branchContactName','Contact Name','required');
			$this->form_validation->set_rules('branchContactTel','Contact Tel','required');
			$this->form_validation->set_rules('branchContactEmail','Contact Email','required');


			if($this->form_validation->run() == false){
				//load create new observation form
				$this->data['title'] = lang('companyBranchCreateHeading');
				$this->data['content'] = 'system/createCompanyBranch';
				$this->data['companyID'] = $companyID;

				$this->load->view('includes/template',$this->data);
			}else{


				//save information and return to listing
				$uploadData['branchName'] = strtoupper($_POST['branchName']);
				$uploadData['companyID'] = $_POST['companyID'];
				$uploadData['branchContactName'] = $_POST['branchContactName'];
				$uploadData['branchContactTel'] = $_POST['branchContactTel'];
				$uploadData['branchContactEmail'] = $_POST['branchContactEmail'];				
				$uploadData['created'] = date('Y-m-d H:i:S');
				$uploadData['createdBy'] = $this->session->userdata('user_id');

				if($this->CompanyProfileModel->createBranchProfile($uploadData)){
				
						redirect("CompanyProfileAdmin/companyBranchListing/{$uploadData['companyID']}");
					
				}else{
					show_404();
				}
			}
		}

		function viewBranch($branchID = null,$companyID = null){
			$this->data['title'] = lang('systemViewCompanyBranchHeading');
			$this->data['content'] = 'system/viewCompanyBranchProfile';
			$this->data['branch'] = $this->CompanyProfileModel->getCompanyBranchProfile($branchID);
			
			$this->data['companyID'] = $companyID;
			$this->load->view('includes/template',$this->data);			

		}

		function editBranch($branchID = null,$companyID = null){
			$this->form_validation->set_rules('branchName','Branch Name','required');
			$this->form_validation->set_rules('branchContactName','Contact Name','required');
			$this->form_validation->set_rules('branchContactTel','Contact Tel','required');
			$this->form_validation->set_rules('branchContactEmail','Contact Email','required');


			if($this->form_validation->run() == false){
				//load create new observation form
				$this->data['title'] = lang('systemcompanyBranchEditHeading');
				$this->data['content'] = 'system/editCompanyBranch';
				$this->data['companyID'] = $companyID;
				$this->data['branch'] = $this->CompanyProfileModel->getCompanyBranchProfile($branchID);

				$this->load->view('includes/template',$this->data);
			}else{


				//save information and return to listing
				$uploadData['branchName'] = strtoupper($_POST['branchName']);
				$uploadData['companyID'] = $_POST['companyID'];
				$uploadData['branchID'] = $_POST['branchID'];
				$uploadData['branchContactName'] = $_POST['branchContactName'];
				$uploadData['branchContactTel'] = $_POST['branchContactTel'];
				$uploadData['branchContactEmail'] = $_POST['branchContactEmail'];				
				$uploadData['lastUpdated'] = date('Y-m-d H:i:S');
				$uploadData['lastUpdatedBy'] = $this->session->userdata('user_id');

				if($this->CompanyProfileModel->editBranchProfile($_POST['branchID'],$uploadData)){
				
						redirect("CompanyProfileAdmin/companyBranchListing/{$uploadData['companyID']}");
					
				}else{
					show_404();
				}
			}			
		}

	}

?>